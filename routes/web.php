<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.index');
// });



// ============================== Index Page ==============================
Route::get("/", "MainController@index")->name('index');

// ============================== Product Page ==============================
Route::get("/san-pham", "ProductsController@index")->name('products');
Route::get("/san-pham/{slug}", "ProductsController@detail")->name('products.detail');

// ============================== Recruitment Page ==============================
Route::get("/tuyen-dung", "RecruitmentController@index")->name('recruitment');
Route::get("/tuyen-dung/tham-gia", "RecruitmentController@join")->name('recruitment.join');

// ============================== Contact Page ==============================
Route::get('/lien-he', "ContactController@index")->name('contact');
Route::post('/lien-he/submit', "ContactController@submit")->name('contact.submit');

// ============================== Cart Page ==============================
Route::get("/gio-hang", "CartController@index")->name('cart');
Route::get("/gio-hang/them/{id}", "CartController@add")->name('cart.add');
Route::get("/gio-hang/show/", "CartController@show")->name('cart.show');
Route::get("/gio-hang/delete/{rowId}", "CartController@delete")->name('cart.delete');
Route::get("/gio-hang/update/{rowId}/{qty}", "CartController@update")->name('cart.update');

// ============================== Checkout Page ==============================
Route::get("/thanh-toan", "CheckoutController@index")->name('checkout');
Route::post("/thanh-toan/mua", "CheckoutController@buy")->name('checkout.buy');

// ============================== Success Page ==============================
Route::get("/dat-hang-thanh-cong", "SuccessController@index")->name('success');


Auth::routes([
    'reset' => false,
    'register' => false
]);

// Admin order
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/admin', "AdminController@index")->name('admin');
Route::get('/admin/view-order/{id}', "AdminController@view_order")->name('admin.view-order');
Route::get('/admin/view-waiting-orders', "AdminController@viewAllWaitingOrders")->name('admin.view-waiting-orders');
Route::get('/admin/view-success-orders', "AdminController@viewAllSuccessOrders")->name('admin.view-success-orders');
Route::get('/admin/view-deleted-orders', "AdminController@viewAllDeletedOrders")->name('admin.view-deleted-orders');
Route::get('/admin/finish-order/{id}', "AdminController@finishOrder")->name('admin.finish-order');
Route::get('/admin/delete-order/{id}', "AdminController@deleteOrder")->name('admin.delete-order');
Route::get('/admin/restore-order/{id}', "AdminController@restoreOrder")->name('admin.restore-order');
Route::get('/admin/forceDelete-order/{id}', "AdminController@forceDeleteOrder")->name('admin.forceDelete');
// Admin product
Route::get('/admin/all-products', "ProductsController@viewAll")->name('admin.viewAllProducts');
Route::get('/admin/view-product/{id}', "ProductsController@viewProduct")->name('admin.viewProduct');
Route::post('/admin/edit-product/{id}', "ProductsController@editProduct")->name('admin.editProduct');
// Admin customers
Route::get('/admin/recruitment-customers/', 'RecruitmentController@getAll')->name('admin.getAllRecruitment');
Route::get('/admin/contact-customers/', 'ContactController@getAll')->name('admin.getAllContact');

//contact
