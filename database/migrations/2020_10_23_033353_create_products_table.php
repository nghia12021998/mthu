<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string("product_name");
            $table->integer("product_price");
            $table->text("product_description");
            $table->string("slug");
            $table->string("product_thumbnail");
            $table->string("product_avatar");
            $table->string("product_image_1");
            $table->string("product_image_2");
            $table->string("product_image_3");
            $table->string("product_image_4");
            $table->string("product_image_5");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
