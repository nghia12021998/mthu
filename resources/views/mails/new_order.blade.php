<div class='wrapper'
    style='text-align: center; width:600px; margin: 0 auto; font-family:Arial, Helvetica, sans-serif; background-color:#f1f1f1; padding: 10px'>
    <img style='width: 100px; margin-top: 50px;' src='{{ asset('images/logo-icon.jpg') }}' alt=''>
    <h3 style='color: green;'>Đơn hàng mới</h3>
    <div style="text-align: center; margin: 30px 0; font-size: 16px">
        Khách hàng <span style='color: green;'>{{ $user->fullname }}</span> đã đặt hàng tại Vijully Cosmetics.
    </div>
    <div style='text-align: left; float:left; margin-top: 20px; margin-bottom:20px; width:50%;'>
        <b>Thông tin đơn hàng</b>
        <p>{{ $user->fullname }}</p>
        <p>{{ $user->email }}</p>
        <p>{{ $user->phoneNumber }}</p>
    </div>

    <div style='text-align: left; float:right; margin-top: 20px; margin-bottom:20px; width:50%;'>
        <b>Địa chỉ giao hàng</b>
        <p>{{ $user->address }}</p>
    </div>

    <div style='clear:both;'>
        <h4>Chi tiết đơn hàng</h4>

        <table style='width:100%; text-align: center; border-spacing: 0px;'>
            <thead>
                <tr style='background-color: green; color:white;'>
                    <th style='padding: 10px 0 10px 5px;'>Sản phẩm</th>
                    <th>Giá</th>
                    <th>Số lượng</th>
                    <th>Tạm tính</th>
                </tr>
            </thead>
            <tbody>
                {{-- <tr style='background-color: #eaeaea;'>
                    <td style='padding: 10px 0 10px 5px;'>Tinh dầu bưởi</td>
                    <td>120.000VNĐ</td>
                    <td>1</td>
                    <td>120.000VNĐ</td>
                </tr> --}}
                @foreach ($products as $product)
                    <tr style='background-color: #eaeaea;'>
                        <td style='padding: 10px 0 10px 5px;'>{{ $product->name }}</td>
                        <td>{{ number_format($product->price, 0, '', '.') }} VND</td>
                        <td>{{ number_format($product->qty, 0, '', '.') }}</td>
                        <td>{{ number_format($product->subtotal, 0, '', '.') }} VND</td>
                    </tr>
                @endforeach

                {{-- <tr style='background-color: #eaeaea;'>
                    <td style='padding: 10px 0 10px 5px;'>Tinh dầu bưởi</td>
                    <td>120.000VNĐ</td>
                    <td>1</td>
                    <td>120.000VNĐ</td>
                </tr> --}}
            </tbody>
            <tfoot style='background-color: #d9d9d9;'>
                <tr>
                    <td colspan='2'></td>
                    <td style='padding: 10px 0 10px 0'>Phí vận chuyển:</td>
                    <td>{{ number_format($shippingFee, 0, '', '.') }} VND</td>
                </tr>
                <tr>
                    <td colspan='2'></td>
                    <td style='padding: 10px 0 10px 0'><b>Tổng giá trị đơn hàng:</b></td>
                    <td>{{ number_format($total, 0, '', '.') }} VND</td>
                </tr>
            </tfoot>
        </table>
    </div>
    <a style='display: inline-block; width: 150px; text-decoration: none; background-color: green; color: white; padding: 8px 0 8px 0; border-radius: 3px; margin: 20px 0;'
        href='{{ route('admin') }}' target='_blank'>Kiểm tra đơn hàng</a>
</div>
