<h4>Khách hàng <span style="color: #2b6b34">{{ $user->fullname }} </span> đã gửi một yêu cầu gia nhập hệ thống.</h4>

<p><i>Tên khách hàng: </i> <b>{{ $user->fullname }}</b></p>
<p><i>Số điện thoại: </i> <b>{{ $user->phoneNumber }}</b></p>
<p><i>Email: </i> <b>{{ $user->email }}</b></p>
<a style='display: inline-block; width: 150px; text-decoration: none; background-color: green; color: white; padding: 8px 0 8px 0; border-radius: 3px; margin: 20px 0;  text-align: center;'
    href='{{ route('admin.getAllRecruitment') }}' target='_blank'>Kiểm tra</a>
