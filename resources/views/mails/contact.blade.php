<h4>Khách hàng <span style="color: #2b6b34">{{ $user->txtFullName }} </span> đã gửi một yêu cầu liên hệ.</h4>

<p><i>Tên khách hàng: </i> <b>{{ $user->txtFullName }}</b></p>
<p><i>Số điện thoại: </i> <b>{{ $user->txtPhoneNumber }}</b></p>
<p><i>Email: </i> <b>{{ $user->txtEmail }}</b></p>
<p>
    <i>Nội dung:</i><br>
    <b>{{ $user->txtContent }}</b>
</p>
<a style='display: inline-block; width: 100px; text-decoration: none; background-color: green; color: white; padding: 8px 0 8px 0; border-radius: 3px; margin: 20px 0; text-align: center;'
    href='{{ route('admin.getAllContact') }}' target='_blank'>Kiểm tra</a>
