@extends('layouts.admin')

@section('content')
    <div class="pt-4">
        <div class="card">
            <div class="card-header text-center bg-warning text-dark font lead">Tất cả khách hàng</div>
            @if (!empty($customers->first()))
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead class="">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Tên khách hàng</th>
                                <th scope="col">Email</th>
                                <th scope="col">Số điện thoại</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($customers as $customer)
                                <tr>
                                    <th scope="col">{{ $customer->id }}</th>
                                    <td>{{ $customer->customer_name }}</td>
                                    <td>{{ $customer->customer_email }}</td>
                                    <td>{{ $customer->customer_phoneNumber }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $customers->links() }}
                </div>
            @else
                <div class="text-center pt-3 pb-3">Không có khách hàng</div>
            @endif
        </div>
    </div>
@endsection
