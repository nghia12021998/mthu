@extends('layouts.admin')

@section('content')
    <div class="pt-4">
        @if (Session::has('msg'))
            <div class="alert alert-success mt-3" role="alert">
                {{ Session::get('msg') }}
            </div>
        @endif
        <div class="card">
            <div class="card-header text-center bg-info text-white font lead">{{ $product->product_name }}</div>
            <div class="card-body">
                {!! Form::open(['url' => route('admin.editProduct', ['id' => $product->id]), 'method' => 'POST']) !!}
                <div class="form-group">
                    {!! Form::label('txtProductName', 'Tên sản phẩm:') !!}
                    {!! Form::text('txtProductName', $product->product_name, ['id' => 'txtProductName', 'class' =>
                    'form-control', 'placeholder' => 'Nhập tên sản phẩm']) !!}
                    @error('txtProductName')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    {!! Form::label('txtProductPrice', 'Giá sản phẩm:') !!}
                    {!! Form::number('txtProductPrice', $product->product_price, ['id' => 'txtProductPrice', 'class' =>
                    'form-control', 'placeholder' => 'Nhập giá sản phẩm']) !!}
                    @error('txtProductPrice')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    {!! Form::label('txtProductDescription', 'Mô tả sản phẩm:') !!}
                    {!! Form::textarea('txtProductDescription', $product->product_description, ['id' =>
                    'txtProductDescription']) !!}
                    @error('txtProductDescription')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                {!! Form::submit('Lưu thay đổi', ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
