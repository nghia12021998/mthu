@extends('layouts.admin')

@section('content')
    <div class="pt-4">
        <div class="card">
            <div class="card-header text-center bg-primary text-light font lead">Đơn hàng mới</div>
            @if (!empty($orders->first()))
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead class="">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Khách hàng</th>
                                <th scope="col">Số lượng SP</th>
                                <th scope="col">Tổng cộng</th>
                                <th scope="col">Ngày mua</th>
                                <th scope="col">Chi tiết</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                                <tr>
                                    <th scope="row">{{ $order->id }}</th>
                                    <td>{{ $order->customer_name }}</td>
                                    <td>{{ $order->ordersDetails->sum('quantity') }}</td>
                                    <td>{{ number_format($order->total, 0, '', '.') }} VND</td>
                                    <td>{{ $order->created_at->format('d/m/Y') }}</td>
                                    <td>
                                        <a href=" {{ route('admin.view-order', $order->id) }} ">
                                            <i class="far fa-edit edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $orders->links() }}
                </div>
            @else
                <div class="text-center pt-3 pb-3">Không có đơn hàng</div>
            @endif

        </div>
    </div>
@endsection
