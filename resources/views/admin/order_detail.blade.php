@extends('layouts.admin')

@section('content')
    <div class="wrapper pt-3">
        <div class="card">
            <h5 class="card-header text-center">Đơn hàng #{{ $order->id }}
                @php
                if($order->deleted_at){
                echo "<span class='text-danger'>(Đã xóa)</span>";
                }else{
                if($order->status == "success"){
                echo "<span class='text-success'>(Hoàn thành)</span>";
                }else{
                echo "<span class='text-primary'>(Mới)</span>";
                }
                }
                @endphp</h5>
            <div class="card-body">
                <div class="container pt-5 pb-5">
                    <div class="row">
                        <div class="col-6">
                            <ul>
                                <li>
                                    <p>
                                        <i>Tên khách hàng:</i> <b>{{ $order->customer_name }}</b>
                                    </p>
                                    <p><i>Email:</i> <b>{{ $order->customer_email }}</b></p>
                                    <p><i>Ngày đặt hàng:</i> <b>{{ $order->created_at }}</b></p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-6">
                            <ul>
                                <li>
                                    <p><i>Số điện thoại:</i> <b>{{ $order->customer_phoneNumber }}</b></p>
                                    <p>
                                        <i>Địa chỉ:</i>
                                        <b>{{ $order->customer_address }}</b>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <h6 class="mt-4">Sản phẩm:</h6>
                    <table class="table table-bordered mb-5">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Tên sản phẩm</th>
                                <th scope="col">Số lượng</th>
                                <th scope="col">Giá</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $t = 1;
                            @endphp
                            @foreach ($details as $detail)
                                <tr>
                                    <th scope="row">{{ $t++ }}</th>
                                    <td>{{ $detail->products->product_name }}</td>
                                    <td>{{ $detail->quantity }}</td>
                                    <td>{{ number_format($detail->quantity * $detail->products->product_price, 0, '', '.') }}
                                        VND</td>
                                </tr>
                            @endforeach


                            <tr>
                                <td colspan="3" class="text-center">Phí giao hàng</td>
                                <td>{{ number_format($order->shipping_fee, 0, '', '.') }} VND</td>
                            </tr>
                            <tr>
                                <td colspan="3" class="text-center">Tổng cộng</td>
                                <td><b>{{ number_format($order->total, 0, '', '.') }} VND</b></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-12 text-center">
                            @if ($order->status == 'waiting' && !$order->deleted_at)
                                <a href=" {{ route('admin.finish-order', ['id' => $order->id]) }} "
                                    class="btn btn-success">Hoàn thành đơn hàng</a>
                            @endif

                            @if ($order->deleted_at)
                                <a href=" {{ route('admin.restore-order', ['id' => $order->id]) }} "
                                    class="btn btn-success">Khôi phục đơn hàng</a>
                                <a href="{{ route('admin.forceDelete', ['id' => $order->id]) }}" class="btn btn-danger">Xóa
                                    vĩnh viễn</a>
                            @else
                                <a href="{{ route('admin.delete-order', ['id' => $order->id]) }}" class="btn btn-danger">Xóa
                                    đơn
                                    hàng</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if (Session::has('msg'))
            <div class="alert alert-success mt-3" role="alert">
                {{ Session::get('msg') }}
            </div>
        @endif

    </div>
@endsection
