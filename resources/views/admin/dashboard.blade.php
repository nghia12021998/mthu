@extends('layouts.admin')

@section('content')
    <div class="row pt-3">
        <div class="col-sm-12 col-md-3">
            <div class="card">
                <div class="card-body text-center">
                    <h5 class="card-title">
                        <i class="far fa-shopping-cart text-primary"></i>
                    </h5>
                    <p class="card-text">{{ $total_waiting_orders }}</p>
                    <h6 class="card-subtitle mb-2 text-muted">Đơn hàng mới</h6>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-3">
            <div class="card">
                <div class="card-body text-center">
                    <h5 class="card-title">
                        <i class="far fa-check text-success"></i>
                    </h5>
                    <p class="card-text">{{ $total_success_orders }}</p>
                    <h6 class="card-subtitle mb-2 text-muted">
                        Đơn hàng hoàn thành
                    </h6>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-3">
            <div class="card">
                <div class="card-body text-center">
                    <h5 class="card-title">
                        <i class="far fa-times text-danger"></i>
                    </h5>
                    <p class="card-text">{{ $total_deleted_orders }}</p>
                    <h6 class="card-subtitle mb-2 text-muted">Đơn hàng đã hủy</h6>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-3">
            <div class="card">
                <div class="card-body text-center">
                    <h5 class="card-title">
                        <i class="far fa-dollar-sign text-success"></i>
                    </h5>
                    <p class="card-text">{{ number_format($income, 0, '', '.') }} VND</p>
                    <h6 class="card-subtitle mb-2 text-muted">Tổng doanh thu</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="card mt-4">
        <div class="card-header text-center bg-primary text-light font lead">Đơn hàng mới</div>

        @if (!empty($orders->first()))
            <div class="card-body">
                <table class="table table-bordered">
                    <thead class="">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Khách hàng</th>
                            <th scope="col">Số lượng SP</th>
                            <th scope="col">Tổng cộng</th>
                            <th scope="col">Ngày mua</th>
                            <th scope="col">Chi tiết</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <th scope="row">{{ $order->id }}</th>
                                <td>{{ $order->customer_name }}</td>
                                <td>{{ $order->ordersDetails->sum('quantity') }}</td>
                                <td>{{ number_format($order->total, 0, '', '.') }} VND</td>
                                <td>{{ $order->created_at->format('d/m/Y') }}</td>
                                <td>
                                    <a href=" {{ route('admin.view-order', $order->id) }} ">
                                        <i class="far fa-edit edit"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        @else
            <div class="text-center pt-3 pb-3">Không có đơn hàng</div>
        @endif
    </div>
@endsection
