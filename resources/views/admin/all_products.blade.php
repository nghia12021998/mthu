@extends('layouts.admin')

@section('content')
    <div class="pt-4">
        <div class="card">
            <div class="card-header text-center bg-info text-white font lead">Tất cả sản phẩm</div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead class="">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Tên sản phẩm</th>
                            <th scope="col">Giá</th>
                            <th scope="col">Chi tiết</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <th scope="row">{{ $product->id }}</th>
                                <td>{{ $product->product_name }}</td>
                                <td>{{ number_format($product->product_price, 0, '', '.') }} VND</td>
                                <td>
                                    <a href=" {{ route('admin.viewProduct', ['id' => $product->id]) }} ">
                                        <i class="far fa-edit edit"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
