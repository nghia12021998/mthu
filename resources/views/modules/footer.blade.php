<footer>
    <div class="container">
        <div class="ft-wrapper">
            <div class="ft-header">
                <h4>VIJULLY COSMETICS</h4>
            </div>
            <div class="ft-intro">
                <p>Best Of Nature<br />Nơi Gửi Gắm Niềm Tin Của Phái Đẹp!</p>
            </div>
        </div>
        <div class="media-icon">
            <a href="https://www.facebook.com/lnminhthu.1512" target="_blank"><i class="fab fa-facebook-f"></i></a>
            <a href="https://www.instagram.com/tinhdaubuoi.vijullycosmetics/?hl=en" target="_blank"><i
                    class="fab fa-instagram"></i></a>
            <!-- <a href="#"><i class="fab fa-youtube"></i></a> -->
            <a href="https://shopee.vn/chuyentinhdaubuoicosmetics?fbclid=IwAR0P24A2GiOUXthN_JP1t2v4jnPbeZ4P-yEBo-Z5UlqKdG8u_0h-5TR2S4E"
                target="_blank"><i class="fal fa-store"></i></a>
            <p>&copy; Copyright 2020 Vijully Cosmetic. All Rights Reserved.</p>
        </div>
    </div>
</footer>

<div id="dark-background"></div>
