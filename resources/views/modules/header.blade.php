<div class="promotions">Miễn phí vận chuyển khi mua từ 2 sản phẩm.</div>
<header>
    <nav>
        <div class="container">
            <div id="nav-wrapper">
                <div id="logo">
                    <a href="{{ route('index') }}">
                        <img src="{{ asset('images/green-logo.png') }}" alt="Vijully Cosmectics" />
                    </a>
                </div>
                <div id="burger">
                    <div class="line line1"></div>
                    <div class="line line2"></div>
                    <div class="line line3"></div>
                </div>
                <ul id="main-nav">
                    <li class="nav-item"><a href="{{ route('index') }}">Trang chủ</a></li>
                    <li class="nav-item"><a href="{{ route('products') }}">Sản phẩm</a></li>
                    <li class="nav-item">
                        <a href="{{ route('recruitment') }}">Tuyển dụng</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('contact') }}">Liên hệ</a>
                    </li>
                    <li id="cart-qty" class="nav-item">
                        <a class="" href="{{ route('cart') }}">Giỏ hàng ( <span>0</span> )</a>
                    </li>
                    <li id="bag-icon" class="nav-item">
                        <i id="" class="fal fa-shopping-bag bag"></i>
                        <span id="bag-qty">0</span>
                        <div id="cart">
                            <div id="loading-bag" class="d-none">
                                <div class="spinner-border"></div>
                            </div>
                            <div id="cart-items">
                                <ul>
                                    <!-- <li class="cart-item">
                                            <a href="">
                                              <div class="product-thumb">
                                                <img
                                                  src="/assets/images/products/xitbuoi-thumb.jpg"
                                                />
                                              </div>
                                              <div class="product-info">
                                                <p class="product-name">Tinh dầu bưởi</p>
                                                <p class="qty">
                                                  Số lượng: <span>1</span> × 200.000VNĐ
                                                </p>
                                              </div>
                                              <div class="product-sub-price">
                                                <p>200.000VNĐ</p>
                                              </div>
                                            </a>
                                          </li>
                                          -->
                                </ul>

                                <div id="cart-btn">
                                    {{-- <div class="view-cart">
                                        <a href="gio-hang">XEM GIỎ HÀNG</a>
                                    </div>

                                    <div class="checkout-cart">
                                        <a href="thanh-toan">THANH TOÁN</a>
                                    </div> --}}
                                </div>
                                {{-- <h4 class="empty-cart">
                                    Giỏ hàng trống! <a href="{{ route('products') }}">Mua hàng »</a>
                                </h4> --}}

                                <div id="cart-subtotal">
                                    <p>Tổng cộng: <span id="subtotal-price">0 VNĐ</span></p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="contact-phone">
        <a href="tel:0961503015">
            <i class="fal fa-phone-alt"></i>
            <h2>0961503015</h2>
        </a>
    </div>
</header>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml: true,
            version: "v7.0",
        });
    };

    (function(d, s, id) {
        var js,
            fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
        fjs.parentNode.insertBefore(js, fjs);
    })(document, "script", "facebook-jssdk");

</script>
<!-- Your Chat Plugin code -->
<div class="fb-customerchat" attribution="setup_tool" page_id="108763090851078" theme_color="#2b6b34"
    logged_in_greeting="Vijully Cosmetics xin chào bạn! Chúng tôi có thể giúp gì được cho bạn ?"
    logged_out_greeting="Vijully Cosmetics xin chào bạn! Chúng tôi có thể giúp gì được cho bạn ?">
</div>
