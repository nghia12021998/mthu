<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="{{ asset('images/logo-icon.jpg') }}" type="image/gif">
    <title>Vijully Cosmetics</title>
    <meta name="description" content="Nơi Đây Giúp Cho Tất Cả Mọi Người Có Một Mái Tóc Hoàn Hảo"/>
    <meta name="keywords" content="vijully, vijully cosmetic, vijully cosmetics, tinh dau buoi, xit buoi vijully"/>
    <meta property="og:url" content="http://vijullycosmetics.com/">
    <meta property="og:type" content="website"/>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Inconsolata:wght@400;700&display=swap" rel="stylesheet" />
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap"
        rel="stylesheet" />
    <!-- FontAwesome -->
    <link rel="stylesheet" href="{{ asset('css/all.css') }}" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <!-- Responsive Style -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" />
    <!-- AOS -->
    <link rel="stylesheet" href="{{ asset('css/aos.css') }}" />
    <!-- Owl-Carousel -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}" />
    <!-- <link rel="stylesheet" href="/assets/css/owl.theme.default.min.css" /> -->
</head>

<body>
    @include('modules.header')

    @yield('content')

    @include('modules.footer')
    <!-- Jquery -->
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <!-- FontAwesome -->
    <script src="{{ asset('js/all.js') }}"></script>
    <!-- Bootstrap -->
    {{-- <script src="/assets/js/bootstrap.js"></script> --}}
    <!-- App -->
    <script src="{{ asset('js/main.js') }}"></script>
    <!-- AOS -->
    <script src="{{ asset('js/aos.js') }}"></script>
    <script>
        AOS.init({
            once: true,
            mirror: true,
        });

    </script>
    <!-- Owl-Carousel -->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!-- Typed -->
    <script src="{{ asset('js/typed.min.js') }}"></script>


    <script>
        if (window.location.pathname == '/') {
            var typed = new Typed("#typed", {
                strings: ["hoàn hảo.", "chắc khỏe.", "bóng mượt."],
                loop: true,
                typeSpeed: 100,
                backSpeed: 50,
                backDelay: 2000,
            });
        }

    </script>


</body>

</html>
