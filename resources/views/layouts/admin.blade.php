<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="{{ asset('images/logo-icon.jpg') }}" type="image/gif">
    <title>Admin</title>
    <!-- FontAwesome -->
    <link rel="stylesheet" href="{{ asset('css/all.css') }}" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}" />
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap"
        rel="stylesheet" />
    <script src="https://cdn.tiny.cloud/1/yc689udjj260bc5vgutbycsn5h06intdpntprpsevezb1rpa/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
</head>

<body>
    <header>
        <div class="container-fluid">
            <div id="logo" class="float-left">
                <a href="{{ route('admin') }}">
                    <img src="{{ asset('images/logo.png') }}" alt="" />
                </a>
            </div>
            <div class="greeting float-right">
                <h6>
                    Xin chào, <span class="username">{{ Auth::user()->name }}</span>
                    <i class="far fa-angle-down"></i>
                </h6>
                <div class="dropdown">
                    <ul>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                                Đăng xuất
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <div id="panels">
        <aside id="left-panel">
            @php
            $module = session('module_ative');
            @endphp
            <nav>
                <ul>
                    <li class="nav-item">
                        <i class="far fa-tachometer-slow" style="color: {{ $module == '' ? '#03a9f3' : '' }}"></i>
                        <a href="{{ route('admin') }}" style="color: {{ $module == '' ? '#03a9f3' : '' }}">Dashboard</a>
                    </li>
                    <li class="nav-item unclickable">
                        <i class="far fa-shopping-cart"
                            style="color: {{ $module == 'waittingOrders' || $module == 'successOrders' || $module == 'successOrders' || $module == 'deletedOrders' ? '#03a9f3' : '' }}"></i>
                        <a href=""
                            style="color: {{ $module == 'waittingOrders' || $module == 'successOrders' || $module == 'successOrders' || $module == 'deletedOrders' ? '#03a9f3' : '' }}">Đơn
                            hàng</a>
                        <i class=" far fa-angle-right float-right angle mt-1 {{ $module == 'waittingOrders' || $module == 'successOrders' || $module == 'successOrders' || $module == 'deletedOrders' ? 'rotate' : '' }}""></i>

                        <ul
                            class=" sub-nav
                            {{ $module == 'waittingOrders' || $module == 'successOrders' || $module == 'successOrders' || $module == 'deletedOrders' ? 'display' : '' }}">
                    <li class="sub-nav-item">
                        <a href="{{ route('admin.view-waiting-orders') }}"
                            style="color: {{ $module == 'waittingOrders' ? '#03a9f3' : '' }}"><i class="far fa-plus"
                                style="color: {{ $module == 'waittingOrders' ? '#03a9f3' : '' }}"></i>Đơn
                            hàng
                            mới</a>
                    </li>
                    <li class="sub-nav-item">

                        <a href="{{ route('admin.view-success-orders') }}"
                            style="color: {{ $module == 'successOrders' ? '#03a9f3' : '' }}"><i class="far fa-check"
                                style="color: {{ $module == 'successOrders' ? '#03a9f3' : '' }}"></i>Đã hoàn
                            thành</a>
                    </li>
                    <li class="sub-nav-item">

                        <a href="{{ route('admin.view-deleted-orders') }}"
                            style="color: {{ $module == 'deletedOrders' ? '#03a9f3' : '' }}"><i class="far fa-times"
                                style="color: {{ $module == 'deletedOrders' ? '#03a9f3' : '' }}"></i>Đã
                            hủy</a>
                    </li>
                </ul>
                </li>
                <li class="nav-item unclickable">
                    <i class="far fa-users"
                        style="color: {{ $module == 'getAllRecruitment' || $module == 'getAllContact' ? '#03a9f3' : '' }}"></i>
                    <a href=""
                        style="color: {{ $module == 'getAllRecruitment' || $module == 'getAllContact' ? '#03a9f3' : '' }}">Khách
                        hàng</a>
                    <i class="far fa-angle-right float-right angle mt-1"
                        style="color: {{ $module == 'getAllRecruitment' || $module == 'getAllContact' ? '#03a9f3' : '' }}"></i>

                    <ul
                        class="sub-nav {{ $module == 'getAllRecruitment' || $module == 'getAllContact' ? 'display' : '' }}">
                        <li class="sub-nav-item ">
                            <a href="{{ route('admin.getAllRecruitment') }}"
                                style="color: {{ $module == 'getAllRecruitment' ? '#03a9f3' : '' }}"><i
                                    class="far fa-user-plus"
                                    style="color: {{ $module == 'getAllRecruitment' ? '#03a9f3' : '' }}"></i>Gia nhập hệ
                                thống</a>
                        </li>
                        <li class="sub-nav-item">
                            <a href="{{ route('admin.getAllContact') }}"
                                style="color: {{ $module == 'getAllContact' ? '#03a9f3' : '' }}"><i class=" far
                                fa-phone-alt" style="color: {{ $module == 'getAllContact' ? '#03a9f3' : '' }}"></i>Liên
                                hệ</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item unclickable">
                    <i class="far fa-boxes" style="color: {{ $module == 'viewAllProducts' ? '#03a9f3' : '' }}"></i>
                    <a href="" style=" color: {{ $module == 'viewAllProducts' ? '#03a9f3' : '' }}">Sản phẩm</a>
                    <i class=" far fa-angle-right float-right angle mt-1"></i>
                    <ul class="sub-nav {{ $module == 'viewAllProducts' ? 'display' : '' }}">
                        <li class="sub-nav-item">
                            <a href="{{ route('admin.viewAllProducts') }}"
                                style=" color: {{ $module == 'viewAllProducts' ? '#03a9f3' : '' }}"><i
                                    class="far fa-edit"
                                    style=" color: {{ $module == 'viewAllProducts' ? '#03a9f3' : '' }}"></i>Chỉnh sửa
                                sản
                                phẩm</a>
                        </li>
                    </ul>
                </li>
                </ul>
            </nav>
        </aside>
        <div id="right-panel">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>


    <!-- Jquery -->
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <!-- FontAwesome -->
    <script src="{{ asset('js/all.js') }}"></script>
    <!-- Bootstrap -->
    {{-- <script src="/assets/js/bootstrap.js"></script> --}}
    <!-- App -->
    <script src="{{ asset('js/admin.js') }}"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            height: 500,
            plugins: [
                'advlist autolink link image lists charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                'table emoticons template paste help'
            ],
            toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
                'bullist numlist outdent indent | ' +
                'forecolor backcolor emoticons',
            content_css: 'css/content.css'
        });

    </script>
</body>

</html>
