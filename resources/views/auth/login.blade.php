{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email"
                                class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin</title>
    <!-- FontAwesome -->
    <link rel="stylesheet" href="{{ asset('css/all.css') }}" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
</head>

<body>
    <form class="login-form" autocomplete="off" action="{{ route('login') }}" method="POST">
        @csrf
        <img src="{{ asset('images/logo.png') }}" alt="Vijully"
            style="width: 200px; display: block; margin: 100px auto" />

        <input type="email" name="email" placeholder="Email" value="{{ old('email') }}" />
        <input type="password" name="password" placeholder="Mật khẩu" />
        <a href="#" class="forgot">Quên mật khẩu?</a>

        @foreach ($errors->all() as $error)
            <p class="error text-danger" style="font-size: 1.7rem">{{ $error }}</p>
        @endforeach

        <button type="submit" name="btn-login" value="Đăng nhập">
            Đăng nhập
        </button>
    </form>
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <!-- FontAwesome -->
    <script src="{{ asset('js/all.js') }}"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    {{-- <script src="/assets/js/app.js"></script> --}}

</body>

</html>
