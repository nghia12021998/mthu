@extends('layouts.main')

@section('content')
    <main id="product-detail-page">
        <div class="container">
            <div class="product-wrapper">
                <div class="product-images">
                    <img src="{{ asset($product->product_image_1) }}" alt="{{ $product->product_name }}"
                        data-aos="fade-right" data-aos-duration="1500" />
                    <img src="{{ asset($product->product_image_2) }}" alt="{{ $product->product_name }}"
                        data-aos="fade-right" data-aos-duration="1500" />
                    <img src="{{ asset($product->product_image_3) }}" alt="{{ $product->product_name }}"
                        data-aos="fade-right" data-aos-duration="1500" />
                </div>
                <div class="wp-product-infomation">
                    <div class="product-infomation" data-aos="fade-left" data-aos-duration="1500">
                        <div class="product-name">
                            <h4>{{ $product->product_name }}</h4>
                        </div>
                        <div class="product-desc">
                            <p>
                                {!! $product->product_description !!}
                            </p>
                        </div>
                        <!-- <div class="product-promotion-head">
                                <h4>ƯU ĐÃI</h4>
                                </div> -->

                        <!-- <div class="product-promotion-content">
                                <p></p>
                                </div> -->

                        <div class="product-page-price">
                            <h4>- {{ number_format($product->product_price, 0, '', '.') }} VND -</h4>
                        </div>

                        <button id="product-btn-add-1" class="product-btn-add" product-id={{ $product->id }}>
                            THÊM VÀO GIỎ HÀNG
                        </button>
                        <button id="product-btn-add-2" class="product-btn-add" product-id={{ $product->id }}>
                            THÊM VÀO GIỎ HÀNG
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
