<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vijully Cosmetics</title>
    <link rel="icon" href="{{ asset('images/logo-icon.jpg') }}" type="image/gif">
    <meta name="description" content="">

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    {{--
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" /> --}}
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Inconsolata:wght@400;700&display=swap" rel="stylesheet">
</head>

<body>
    <div id="success">
        <div class="container">
            <div class="wp">
                <a href="{{ route('index') }}"><img src="{{ asset('images/logo-icon.jpg') }}" alt="logo"></a>
                <h1>Đặt Hàng Thành Công!</h1>
                <h5>Cám ơn <span>{{ session('fullname') }}</span> đã tin tưởng và sử dụng sản phẩm của chúng tôi.
                </h5>
                <a href="{{ route('index') }}" class="go-back">Quay lại trang chủ »</a>
            </div>

        </div>
    </div>

</body>

</html>
