@extends('layouts.main')

@section('content')
    <main id="cart-page" data-aos="zoom-in" data-aos-duration="1000">
        <div class="container position-relative">
            <div id="loading-cart" class="d-none">
                <div class="spinner-border"></div>
            </div>
            @if (!empty($product_list->first()))
                <table id="cart-table">
                    <thead>
                        <tr>
                            <th class="th-product-remove"></th>
                            <th class="th-product-thumb"></th>
                            <th class="th-product-name">Sản phẩm</th>
                            <th class="th-product-price">Giá</th>
                            <th class="th-product-quantity">Số lượng</th>
                            <th class="th-product-subtotal">Tổng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($product_list as $product)
                            <tr>
                                <td class="td-product-remove">
                                    <a href="{{ route('cart.delete', $product->rowId) }}"><i class="fal fa-times"></i></a>
                                </td>
                                <td class="td-product-thumb">
                                    <a href="{{ route('products.detail', $product->options['slug']) }}"><img
                                            src="{{ asset($product->options['thumbnail']) }}"
                                            alt="{{ $product->name }}" /></a>
                                </td>
                                <td class="td-product-name">
                                    <a
                                        href="{{ route('products.detail', $product->options['slug']) }}">{{ $product->name }}</a>
                                </td>
                                <td class="td-product-price">{{ number_format($product->price, 0, '', '.') }} VND</td>
                                <td class="td-product-quantity">
                                    <input class="td-product-qty" type="number" data-id="{{ $product->rowId }}"
                                        value="{{ $product->qty }}" min="1" />
                                </td>
                                <td id="{{ $product->rowId }}" class="td-product-subtotal">
                                    {{ number_format($product->subtotal, 0, '', '.') }}
                                    VND
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                <div class="wp-total">
                    <div class="discound">
                        <!-- <form action="">
                                                                                                                    <input type="text" placeholder="Mã giảm giá" />
                                                                                                                    <button><a href="">ÁP DỤNG</a></button>
                                                                                                                    </form> -->
                    </div>

                    <div class="cart-collaterals">
                        <table>
                            <tbody>
                                <tr>
                                    <th>Tạm tính</th>
                                    <td id="products-subtotal">{{ number_format($subtotal, 0, '', '.') }} VND</td>
                                </tr>
                                <tr>
                                    <th>Shipping</th>
                                    <td class="shipping">

                                        {{ number_format($shipping_fee, 0, '', '.') }} VND
                                        @if ($shipping_fee != 0)
                                            <p class="free-shipping">Freeship khi mua từ 2 sản phẩm.</p>
                                        @endif

                                    </td>
                                </tr>
                                <tr>
                                    <th>Tổng cộng</th>
                                    <td id="products-total">{{ number_format($total, 0, '', '.') }} VND</td>
                                </tr>
                            </tbody>
                        </table>
                        <button class="checkout-btn">
                            <a href="{{ route('checkout') }}">THANH TOÁN</a>
                        </button>
                    </div>
                </div>
            @else
                <h3 class='empty-cart'>Giỏ hàng trống! <a href='san-pham'>Mua hàng »</a></h3>
            @endif
        </div>
    </main>

@endsection
