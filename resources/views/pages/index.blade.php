@extends('layouts.main')
@section('content')
    <main>
        <div class="container">
            <section id="intro">
                <h4>
                    Nơi đây giúp cho tất cả mọi người có một mái tóc
                    <span id="typed"></span>
                </h4>
            </section>

            <section id="products">
                <div class="head-title">
                    <h4>Tất cả sản phẩm</h4>
                </div>
                <ul id="all-products">
                    @php
                    // AOS Effect
                    $aos = ["data-aos=fade-right data-aos-duration=1500", "data-aos=fade-up data-aos-duration=1500",
                    "data-aos=fade-left data-aos-duration=1500"];
                    $t = 0;
                    @endphp

                    @foreach ($products as $product)
                        <li class="" {{ $aos[$t] }}>
                            <a href="{{ route('products.detail', $product->slug) }}">
                                <div class="product">
                                    <img src="{{ asset($product->product_avatar) }}" alt="{{ $product->product_name }}" />
                                    <div class="product-info">
                                        <div class="product-title">
                                            <p>{{ $product->product_name }}</p>
                                        </div>
                                        <div class="product-price">
                                            <p>{{ number_format($product->product_price, 0, '', '.') }} VND</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        @php
                        $t++;
                        @endphp
                    @endforeach

                </ul>
            </section>

            <section id="assurance">
                <div class="head-title">
                    <h4>Đảm Bảo Tiêu Chuẩn</h4>
                    <p class="assurance-intro">
                        Sản phẩm được sản xuất 100% từ tự nhiên, không chất bảo quản, thấy
                        rõ kết quả sau 2 tuần sử dụng. Vijully Cosmetics cam kết cung cấp
                        những sản phẩm có chất lượng tốt nhất cho người dùng.
                    </p>
                </div>
                <div id="assurance-cards" class="" data-aos="fade-up" data-aos-duration="1500" data-aos-once="true">
                    <ul>
                        <li>
                            <i class="fal fa-leaf"></i>
                            <div class="assurance-info">
                                <h6>100% TỰ NHIÊN</h6>
                            </div>
                        </li>
                        <li>
                            <i class="fal fa-vial"></i>
                            <div class="assurance-info">
                                <h6>KHÔNG CHỨA CHẤT HÓA HỌC</h6>
                            </div>
                        </li>
                        <li>
                            <i class="fal fa-thumbs-up"></i>
                            <div class="assurance-info">
                                <h6>HÀNG TRIỆU NGƯỜI TIN DÙNG</h6>
                            </div>
                        </li>
                        <li>
                            <i class="fal fa-users"></i>
                            <div class="assurance-info">
                                <h6>PHÙ HỢP TẤT CẢ MỌI NGƯỜI</h6>
                            </div>
                        </li>
                        <li>
                            <i class="fal fa-heart"></i>
                            <div class="assurance-info">
                                <h6>MAKE BY HEART</h6>
                            </div>
                        </li>
                        <li>
                            <i class="fal fa-globe-asia"></i>
                            <div class="assurance-info">
                                <h6>PHÂN PHỐI TOÀN QUỐC</h6>
                            </div>
                        </li>
                        <li>
                            <i class="fal fa-truck"></i>
                            <div class="assurance-info">
                                <h6>GIAO HÀNG MIỄN PHÍ TOÀN QUỐC</h6>
                            </div>
                        </li>
                        <li>
                            <i class="fal fa-user-plus"></i>
                            <div class="assurance-info">
                                <h6>TUYỂN HỆ THỐNG TOÀN QUỐC</h6>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>

            <section id="feedback">
                <div class="head-title">
                    <h4>Khách Hàng Đánh Giá</h4>
                </div>
                <div class="owl-carousel owl-theme">
                    <div class="owl-carousel-items">
                        @foreach ($feedbacks as $feedback)
                            <div class="owl-carousel-item">
                                <div class="fb-content">
                                    <p>
                                        "{{ $feedback->content }}"
                                    </p>
                                    <img class="fb-img" src="{{ asset($feedback->image) }}" alt="" />
                                </div>
                                <div class="fb-user">- {{ $feedback->name }} -</div>
                            </div>
                        @endforeach
                    </div>
                    <div class="owl-carousel-items">
                        @foreach ($feedbacks_2 as $feedback)
                            <div class="owl-carousel-item">
                                <div class="fb-content">
                                    <p>
                                        "{{ $feedback->content }}"
                                    </p>
                                    <img class="fb-img" src="{{ asset($feedback->image) }}" alt="" />
                                </div>
                                <div class="fb-user">- {{ $feedback->name }} -</div>
                            </div>
                        @endforeach
                    </div>
                    <div class="owl-carousel-items">
                        @foreach ($feedbacks_3 as $feedback)
                            <div class="owl-carousel-item">
                                <div class="fb-content">
                                    <p>
                                        "{{ $feedback->content }}"
                                    </p>
                                    <img class="fb-img" src="{{ asset($feedback->image) }}" alt="" />
                                </div>
                                <div class="fb-user">- {{ $feedback->name }} -</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection
