@extends('layouts.main')

@section('content')
    <main class="contact-page">
        <div class="container">
            <h1 class="page-title">Liên hệ với chúng tôi</h1>
            <div class="row">
                <div class="col-lg-5">
                    <section class="informations">
                        <ul>
                            <li>
                                <i class="fal fa-phone-alt"></i>
                                0961503015
                            </li>
                            <li>
                                <i class="fal fa-map-marker-alt"></i>
                                158/38 Trần Huy Liệu, Phường 8, Quận Phú Nhuận, Thành phố Hồ Chí Minh.
                            </li>
                            <li>
                                <i class="fal fa-clock"></i>
                                24/12
                            </li>
                            <li>
                                <i class="fal fa-envelope"></i>
                                vijully.cosmetics@gmail.com
                            </li>
                        </ul>

                    </section>

                    <section class="media-icon">
                        <a href="https://www.facebook.com/lnminhthu.1512" target="_blank"><i
                                class="fab fa-facebook-f"></i></a>
                        <a href="https://www.instagram.com/tinhdaubuoi.vijullycosmetics/?hl=en" target="_blank"><i
                                class="fab fa-instagram"></i></a>
                        <!-- <a href="#"><i class="fab fa-youtube"></i></a> -->
                        <a href="https://shopee.vn/chuyentinhdaubuoicosmetics?fbclid=IwAR0P24A2GiOUXthN_JP1t2v4jnPbeZ4P-yEBo-Z5UlqKdG8u_0h-5TR2S4E"
                            target="_blank"><i class="fal fa-store"></i></a>
                    </section>
                </div>
                <div class="col-lg-7">
                    <section class="contact">
                        {!! Form::open(['url' => route('contact.submit'), 'method' => 'POST', 'id' => 'contact-form']) !!}
                        <h3>THÔNG TIN CỦA BẠN</h3>
                        @foreach ($errors->all() as $error)
                            <p class="error text-danger" style="font-size: 1.7rem">{{ $error }}</p>
                        @endforeach
                        @if (Session::has('msg'))
                            <p class="error text-success" style="font-size: 1.7rem">{{ Session::get('msg') }}</p>
                        @endif
                        <div class="input-wrap">
                            {!! Form::text('txtFullName', '', ['placeholder' => 'Họ và tên', 'class' => 'txtFullName']) !!}
                            {!! Form::text('txtPhoneNumber', '', ['placeholder' => 'Số điện thoại', 'class' =>
                            'txtPhoneNumber']) !!}
                            {!! Form::email('txtEmail', '', ['placeholder' => 'Email', 'class' => 'txtEmail']) !!}

                            {!! Form::textarea('txtContent', '', ['placeholder' => 'Ghi chú']) !!}
                            {!! Form::submit('Xác Nhận', ['name' => 'btn-submit', 'class' => 'btnContact', 'onclick' =>
                            'contact()']) !!}
                        </div>
                        {!! Form::close() !!}
                    </section>

                </div>
            </div>
        </div>
    </main>

@endsection
