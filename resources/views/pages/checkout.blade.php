<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Vijully Cosmectics</title>
    <!-- Icon -->
    <link rel="icon" href="{{ asset('images/logo-icon.jpg') }}" type="image/gif">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Inconsolata:wght@400;700&display=swap" rel="stylesheet" />
    <!-- FontAwesome -->
    <link rel="stylesheet" href="{{ asset('css/all.css') }}" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <!-- Responsive Style -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" />
</head>


<body style="overflow-x: hidden">
    <main id="checkout-wrapper">
        <div class="container">
            <div class="logo">
                <a href="{{ route('index') }}"><img src="{{ asset('images/green-logo.png') }}" alt="" /></a>
            </div>
            <aside>
                <div class="cart">
                    <ul>
                        @foreach ($product_list as $product)
                            <li>
                                <div class="product-thumb">
                                    <img src="{{ asset($product->options['thumbnail']) }}" alt="{{ $product->name }}" />
                                </div>
                                <div class="product-infomations">
                                    <p class="product-name">
                                        <a
                                            href="{{ route('products.detail', $product->options['slug']) }}">{{ $product->name }}</a>
                                    </p>
                                    <p class="qty">Số lượng: {{ $product->qty }} ×
                                        {{ number_format($product->price, 0, '', '.') }} VND
                                    </p>
                                </div>
                                <div class="product-sub-price">
                                    <p>{{ number_format($product->subtotal, 0, '', '.') }} VND</p>
                                </div>
                            </li>
                        @endforeach

                    </ul>
                </div>
                <div class="discount clearfix">
                    <form>
                        <input type="text" placeholder="Mã giảm giá" />
                        <input type="button" value="Áp Dụng" />
                    </form>
                </div>

                <div class="subtotal">
                    <table>
                        <tbody>
                            <tr>
                                <th>Tạm tính</th>
                                <td>{{ number_format($subtotal, 0, '', '.') }} VND</td>
                            </tr>
                            <tr>
                                <th>Shipping</th>
                                <td class="shipping">
                                    {{ number_format($shipping_fee, 0, '', '.') }} VND
                                    @if ($shipping_fee != 0)
                                        <p class="free-shipping">Freeship khi mua từ 2 sản phẩm.</p>
                                    @endif

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="total">
                    <table>
                        <tbody>
                            <tr>
                                <th>Tổng cộng</th>
                                <td>{{ number_format($total, 0, '', '.') }} VND</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </aside>

            <main>
                <div class="logo">
                    <a href="{{ route('index') }}"><img src="{{ asset('images/green-logo.png') }}" alt="" /></a>
                </div>

                <div class="custom-info">
                    <div class="head-info">
                        <h5>Thông tin giao hàng</h5>
                    </div>
                    {{-- <form action="{{ route('checkout.buy') }}" method="POST">
                        @csrf
                        <input type="text" name="fullname" placeholder="Họ và tên" value="" />
                        <input type="text" name="phoneNumber" placeholder="Số điện thoại" value="" />
                        <input type="email" name="email" placeholder="Email" value="" />
                        <input type="text" name="address" placeholder="Địa chỉ giao hàng" value="" />
                        <a href="{{ route('cart') }}"><i class="fal fa-chevron-left"></i> Quay lại giỏ hàng</a>
                        <input type="submit" name="btn-submit" value="Xác Nhận" />
                    </form> --}}
                    {{-- @if ($errors->any())
                        <p class="error text-danger" style="font-size: 1.7rem">* Vui lòng nhập đầy đủ thông tin!</p>
                    @endif --}}

                    @foreach ($errors->all() as $error)
                        <p class="error text-danger" style="font-size: 1.7rem">{{ $error }}</p>
                    @endforeach

                    {!! Form::open(['url' => route('checkout.buy'), 'method' => 'POST', 'id' => 'checkout-form']) !!}

                    {!! Form::text('fullname', '', ['placeholder' => 'Họ và tên']) !!}
                    {!! Form::text('phoneNumber', '', ['placeholder' => 'Số điện thoại']) !!}
                    {!! Form::email('email', '', ['placeholder' => 'Email']) !!}
                    {!! Form::text('address', '', ['placeholder' => 'Địa chỉ giao hàng']) !!}
                    <a href="{{ route('cart') }}"><i class="fal fa-chevron-left"></i> Quay lại giỏ hàng</a>
                    {!! Form::submit('Xác Nhận', ['name' => 'btn-submit', 'id' => 'check-out-btn', 'onClick' =>
                    'checkout()']) !!}

                    {!! Form::close() !!}
                </div>
            </main>

            <div class="clearfix"></div>
        </div>
    </main>
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <!-- Jquery -->
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>


    <!-- FontAwesome -->
    <script src="{{ asset('js/all.js') }}"></script>
    <!-- Bootstrap -->
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    <!-- Main -->
    <script src="{{ asset('js/main.js') }}"></script>
</body>

</html>
