@extends('layouts.main')

@section('content')
    <main>
        <div class="container">
            <section class="banner">
                <h1 class="page-title">Tuyển hệ thống trong và ngoài nước</h1>
                <img src="{{ asset('images/company.jpg') }}" alt="vijully" data-aos="fade-up" data-aos-duration="1500">
            </section>

            <section class="benefits">
                <h1 class="main-title">- Quyền Lợi -</h1>
                <h2>Các Chính Sách & Quyền Lợi Khi Gia Nhập Công Ty Vi Jully Cosmetics:</h2>
                <div class="wrapper">
                    <div class="left" data-aos="fade-right" data-aos-duration="1500">
                        <ul>
                            <li>- Đào tạo KDOL chuyên nghiệp từ cơ bản đến nâng cao</li>
                            <li>- Hỗ trợ Freeship</li>
                            <li>- Hỗ trợ Bao bì, băng keo, quà tặng khách</li>
                            <li>- Hỗ trợ Thẻ cảm ơn khách hàng</li>
                            <li>- Khách hàng ( Pass đơn theo khu vực và ngoài khu vực,<br> Pass hệ thống đào tạo</li>
                        </ul>
                    </div>
                    <div class="right" data-aos="fade-left" data-aos-duration="1500">
                        <ul>
                            <li>- Cung cấp Giấy tờ, chứng từ chứng minh nguồn gốc xuất xứ rõ ràng</li>
                            <li>- Thưởng doanh số hàng tháng + thưởng gia nhập + thưởng thăng <br> cấp + thưởng tiền mặt &
                                nếu bán tốt...</li>
                            <li>- Thu hồi hàng & hoàn vốn 100% nếu sau 2 tháng không bán được chỉ khi gia nhập</li>
                            <li>- Được dự Event công ty mỗi quý mỗi năm</li>
                        </ul>
                    </div>
                </div>
                </p>
                </sector>
                <section class="banner">
                    <img src="{{ asset('images/company2.jpg') }}" alt="vijully" data-aos="fade-up" data-aos-duration="1500">
                </section>
                <section class="ranks">
                    <h1 class="main-title">- Hệ Thống Cấp Bậc -</h1>
                    <ul class="rank-list" data-aos="fade-down" data-aos-duration="1500">
                        <li class="rank-item">
                            <h3>Chi Nhánh</h3>
                            <ul>
                                <li>- Vốn gia nhập: 2.000.000₫</li>
                                <li>- Thưởng nóng: 1 Hair Lotion 120.000₫</li>
                            </ul>
                        </li>
                        <li class="rank-item">
                            <h3>Đại Lý</h3>
                            <ul>
                                <li>- Vốn gia nhập: 7.000.000₫</li>
                                <li>- Thưởng nóng: 280.000₫ & 1 Hair Lotion 120.000₫</li>
                            </ul>
                        </li>
                        <li class="rank-item">
                            <h3>Tổng Đại Lý</h3>
                            <ul>
                                <li>- Vốn gia nhập: 25.000.000₫</li>
                                <li>- Thưởng nóng: 1.000.000₫ & 1 Dầu Gội Bưởi 280.000₫</li>
                            </ul>
                        </li>
                        <li class="rank-item">
                            <h3>Nhà Phân Phối</h3>
                            <ul>
                                <li>- Vốn gia nhập: 55.000.000₫</li>
                                <li>- Thưởng nóng: 2.200.000₫ & 1 Combo Bưởi 600.000₫</li>
                            </ul>
                        </li>
                    </ul>

                    <h2>ƯU ĐÃI GIA NHẬP DUY NHẤT TRONG HÔM NAY!</h2>
                </section>

                <section class="contact">
                    <h1 class="main-title">- Đăng Ký -</h1>
                    {{-- <form action="" method="POST">
                        <h3>GIA NHẬP NGAY ĐỂ NHẬN ƯU ĐÃI</h3>
                        <div class="input-wrap">
                            <input type="text" name="txtFullName" placeholder="Họ và tên" required>
                            <input type="text" name="txtPhoneNumber" placeholder="Số điện thoại" required>
                            <input type="email" name="txtEmail" placeholder="Email" required>
                        </div>

                        <button class="btnSubmit" type="submit" name="submit" value="submit">Xác Nhận</button>
                    </form> --}}

                    {!! Form::open(['url' => route('recruitment.join'), 'method' => 'POST', 'id' => 'joinForm']) !!}
                    <h3>GIA NHẬP NGAY ĐỂ NHẬN ƯU ĐÃI</h3>
                    <div class="errorsContainer">

                    </div>
                    <div class="input-wrap">
                        {!! Form::text('txtFullName', '', ['placeholder' => 'Họ và tên', 'class' => 'txtFullName']) !!}
                        {!! Form::text('txtPhoneNumber', '', ['placeholder' => 'Số điện thoại', 'class' =>
                        'txtPhoneNumber']) !!}
                        {!! Form::email('txtEmail', '', ['placeholder' => 'Email', 'class' => 'txtEmail']) !!}
                        {!! Form::submit('Xác Nhận', ['name' => 'submit', 'class' => 'btnJoin']) !!}
                    </div>
                    {!! Form::close() !!}
                </section>
        </div>
    </main>
@endsection
