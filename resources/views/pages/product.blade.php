@extends('layouts.main')
@section('content')
    <main id="product-page">
        <div class="container">
            <section id="products">
                <div class="head-title">
                    <h4>Tất cả sản phẩm</h4>
                </div>

                <ul id="product-list" data-aos="fade-up" data-aos-duration="1500">
                    @foreach ($products as $product)
                        <li>
                            <a href="{{ route('products.detail', $product->slug) }}">
                                <div class="product">
                                    <img src="{{ asset($product->product_avatar) }}" alt="{{ $product->product_name }}" />
                                    <div class="product-info">
                                        <div class="product-title">
                                            <p>{{ $product->product_name }}</p>
                                        </div>
                                        <div class="product-price">
                                            <p>{{ number_format($product->product_price, 0, '', '.') }} VND</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    @endforeach

                </ul>
            </section>
        </div>
    </main>
@endsection
