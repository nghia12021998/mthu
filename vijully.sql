-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 08, 2022 at 04:37 AM
-- Server version: 10.2.41-MariaDB-cll-lve
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ptckietc_vijully`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phoneNumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customer_name`, `customer_phoneNumber`, `customer_email`, `contact_content`, `action`, `created_at`, `updated_at`) VALUES
(1, 'abc', '0123456789', 'abc123@gmail.com', NULL, 'recruitment', '2021-07-04 13:42:52', '2021-07-04 13:42:52');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `name`, `content`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Phương Hằng ( Gạo Nếp Gạo Tẻ )', 'Bé iu ơi ship chị thêm 2 bộ Combo Gội Xịt Xả nữa nha , đợt trước mua xài tốt quá . Chị đặt 1 bộ xài còn bạn chị qua nhà lấy đẹp với chị bảo dùng tốt nên nó nhờ chị mua . Dùng thích lắm em tóc khoẻ hẳn không còn rụng nữa , xài tốt mà bao bì còn sang trọng nữa hí hí', '/images/feedbacks/fb-1.jpg', NULL, NULL),
(2, 'Yến Tatoo', 'Có bạn nào tóc cứ bị rụng , gội xong vuốt ra cả nắm tóc như mình không ? sau thời gian sử dụng thấy ok quá nên giới thiệu đến mn . Sau khi dùng trọn bộ Combo Vi Jully tóc mình đã giảm bớt rụng hẳn , tóc mọc lên rất nhanh mà ủ dừa để xả còn thơm nức mũi và mượt tóc cực kì . Cả bộ xịn sò như này mà mình mua chỉ giá 600.000vnđ , cầm trên tay cứ tưởng tiền triệu vì đẹp và tinh tế', '/images/feedbacks/fb-2.jpg', NULL, NULL),
(3, 'Ly Phan', 'Lần nào mở ask các bạn cũng rất hay hỏi cách chăm sóc tóc , nên hôm nay Ly chia sẻ bộ Dầu Gội , Dầu Xả , Xịt Bưởi mình dùng đã 1 thời gian dài nhé . Ngày xưa cắt tóc ngắn xong tẩy tóc nhuộm loạn xạ nên có giai đoạn tóc cứ lưng lơ k thể dài nổi toàn phải kẹp tóc gủa . Rồi biết đến bộ sản phẩm này và bây giờ tóc dài cực kì và khoẻ nữa , giờ ko bao giờ kẹp tóc giả nữa luôn', '/images/feedbacks/fb-3.jpg', NULL, NULL),
(4, 'Huyền Trang Bất Hối', 'Chắc em ko biết chứ chị sau khi sinh tóc rụng mà tưởng bệnh ko á , rụng quá trời vuốt là rụng . Bạn chị thấy thương quá tặng chị bộ sản phẩm bên em nè . Sau khi sử dụng hết combo tóc c giảm rụng hẳn á Thư . Chỉ còn vài ba cọng như gội thôi , mà còn mượt với khoẻ nữa . Thích ghê vậy đó . Mai nhớ giao cho chị 2 hộp địa chỉ nhà chị nha', '/images/feedbacks/fb-4.jpg', NULL, NULL),
(5, 'Lê Thị Khánh Huyền', 'Chị ơi chai Xịt Bưởi 120k thôi mà vi diệu dữ chị , tóc em bị ít tóc ở chỏm mái vậy mà xịt mỗi ngày 2 lần đến nay cũng 4 tuần tóc con em mọc lên chỗ đó rồi á chị , chắc em sẽ kiên trì sử dụng đều đặn liên tục để mọc kín phần hói ấy . Em chúc sản phẩm chị ngày một phát triển nha', '/images/feedbacks/fb-5.jpg', NULL, NULL),
(6, 'Nguyễn Thảo', 'Chị ơi bắt đền chị á , tóc em trước khi sử dụng Hair Lotion và Dầu Gội Bưởi tóc rụng quá trời mà sau 3 tuần sử dụng tóc em giảm rụng hẳn luôn nè chị ơi , tóc con thì mọc như chôm chôm thích quá đi .Bắt đền chị ra sản phẩm hiệu quả quá đó kakaka. Em sẽ còn ủng hộ chị dài dài đó nha hihi', '/images/feedbacks/fb-6.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(27, '2020_10_23_033353_create_products_table', 2),
(28, '2020_10_28_131626_create_orders_table', 2),
(29, '2020_10_28_131901_create_orders_details_table', 2),
(32, '2020_11_04_141111_create_customers_table', 3),
(33, '2020_11_04_153758_create_feedbacks_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phoneNumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_fee` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_name`, `customer_phoneNumber`, `customer_email`, `customer_address`, `shipping_fee`, `total`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Phan Trần Chí Kiệt', '0973020504', 'kietphan2909@gmail.com', '158/38 Trần Huy Liệu, phường 8, Phú Nhuận, Thành phố Hồ Chí Minh, Việt Nam', 15000, 615000, 'success', NULL, '2021-04-05 13:37:59', '2021-06-20 10:53:46'),
(2, 'Phan Tran Chi Kiet', '0973020504', 'kietphan2909@gmail.com', '15/107a ( số cũ : 46/1 ) Đường Trần Quốc Toản . Khu phố 4 . Phường An Bình . Biên Hòa - Đồng Nai', 15000, 135000, 'success', NULL, '2021-04-05 16:56:19', '2021-06-20 10:53:41'),
(3, 'Phan Tran Chi Kiet', '0973020504', 'kietphan2909@gmail.com', '15/107a ( số cũ : 46/1 ) Đường Trần Quốc Toản . Khu phố 4 . Phường An Bình . Biên Hòa - Đồng Nai', 15000, 615000, 'success', NULL, '2021-04-06 15:02:01', '2021-04-09 09:00:26'),
(4, 'Thu Thuỷ', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 0, 400000, 'success', NULL, '2021-06-20 10:21:58', '2021-06-20 10:53:21'),
(5, 'Thuỷ Trang', '0973776556', 'lnminhthu151220@gmail.com', 'abc dhb dhc', 15000, 615000, 'success', NULL, '2021-06-20 10:22:36', '2021-06-20 10:49:52'),
(6, 'Mai Ngọc', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 0, 360000, 'success', NULL, '2021-06-20 10:24:51', '2021-06-20 10:49:48'),
(7, 'An Nhiên', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 15000, 215000, 'success', NULL, '2021-06-20 10:25:58', '2021-06-20 10:49:44'),
(8, 'Mai Ngọc', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 15000, 615000, 'success', NULL, '2021-06-20 10:27:12', '2021-06-20 10:49:40'),
(9, 'Thuỷ Trang', '0973776556', 'lnminhthu151220@gmail.com', 'abc dhb dhc', 15000, 295000, 'success', NULL, '2021-06-20 10:31:32', '2021-06-20 10:49:36'),
(10, 'Bích Thủy', '0973776556', 'lnminhthu151220@gmail.com', 'abc dhb dhc', 0, 560000, 'success', NULL, '2021-06-20 10:32:02', '2021-06-20 10:49:30'),
(11, 'Quỳnh Anh', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 0, 320000, 'success', NULL, '2021-06-20 10:32:58', '2021-06-20 10:49:24'),
(12, 'Bảo Vy', '0973776556', 'lnminhthu151220@gmail.com', 'abc dhb dhc', 15000, 615000, 'success', NULL, '2021-06-20 10:33:40', '2021-06-20 10:49:20'),
(13, 'Hiền Nhi', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 0, 480000, 'success', NULL, '2021-06-20 10:34:46', '2021-06-20 10:49:16'),
(14, 'Thu Thuỷ', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 15000, 615000, 'success', NULL, '2021-06-20 10:37:11', '2021-06-20 10:46:22'),
(15, 'Thu Thuỷ', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 15000, 295000, 'success', NULL, '2021-06-20 10:37:46', '2021-06-20 10:46:16'),
(16, 'Thu Thuỷ', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 0, 560000, 'success', NULL, '2021-06-20 10:38:23', '2021-06-20 10:46:10'),
(17, 'Bảo Vy', '0973776556', 'lnminhthu151220@gmail.com', 'abc dhb dhc', 0, 400000, 'success', NULL, '2021-06-20 10:39:07', '2021-06-20 10:45:56'),
(18, 'Phương Linh', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 0, 600000, 'success', NULL, '2021-06-20 10:40:45', '2021-06-20 10:45:51'),
(19, 'Phương Linh', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 15000, 295000, 'success', NULL, '2021-06-20 10:43:07', '2021-06-20 10:45:46'),
(20, 'Mai Ngọc', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 0, 800000, 'success', NULL, '2021-06-20 10:44:03', '2021-06-20 10:45:39'),
(21, 'Bảo Vy', '0973776556', 'lnminhthu151220@gmail.com', 'abc dhb dhc', 15000, 615000, 'success', NULL, '2021-06-20 10:45:13', '2021-06-20 10:45:32'),
(22, 'Khánh', '0961503015', 'lnminhthu151220@gmail.com', 'jshsftuwmkdonjsd', 0, 840000, 'success', NULL, '2021-06-20 10:59:53', '2021-06-20 11:12:08'),
(23, 'Khánh', '0961503015', 'lnminhthu151220@gmail.com', 'jshsftuwmkdonjsd', 0, 1200000, 'success', NULL, '2021-06-20 11:01:11', '2021-06-20 11:12:03'),
(24, 'Mai Ngọc', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 0, 400000, 'success', NULL, '2021-06-20 11:04:44', '2021-06-20 11:11:59'),
(25, 'Bảo Vy', '0973776556', 'lnminhthu151220@gmail.com', 'abc dhb dhc', 15000, 135000, 'success', NULL, '2021-06-20 11:06:00', '2021-06-20 11:11:55'),
(26, 'Mai Ngọc', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 15000, 615000, 'success', NULL, '2021-06-20 11:07:09', '2021-06-20 11:11:51'),
(27, 'Phương Linh', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 15000, 615000, 'success', NULL, '2021-06-20 11:07:41', '2021-06-20 11:11:44'),
(28, 'Phương Linh', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 15000, 295000, 'success', NULL, '2021-06-20 11:08:02', '2021-06-20 11:11:39'),
(29, 'Bảo Vy', '0973776556', 'lnminhthu151220@gmail.com', 'abc dhb dhc', 0, 320000, 'success', NULL, '2021-06-20 11:09:17', '2021-06-20 11:11:35'),
(30, 'Khánh', '0961503015', 'lnminhthu151220@gmail.com', 'jshsftuwmkdonjsd', 0, 400000, 'success', NULL, '2021-06-20 11:10:30', '2021-06-20 11:11:26'),
(31, 'Hiền Nhi', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 15000, 215000, 'success', NULL, '2021-06-20 11:10:54', '2021-06-20 11:11:19'),
(32, 'Phương Linh', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 15000, 615000, 'success', NULL, '2021-06-20 11:12:44', '2021-06-27 22:16:17'),
(33, 'Hiền Nhi', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 15000, 295000, 'success', NULL, '2021-06-20 11:13:06', '2021-06-27 22:16:12'),
(34, 'Bảo Vy', '0973776556', 'lnminhthu151220@gmail.com', 'abc dhb dhc', 0, 400000, 'success', NULL, '2021-06-20 11:13:34', '2021-06-27 22:16:08'),
(35, 'Khánh', '0961503015', 'lnminhthu151220@gmail.com', 'jshsftuwmkdonjsd', 0, 560000, 'success', NULL, '2021-06-20 11:13:57', '2021-06-27 22:16:02'),
(36, 'Phương Linh', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 0, 240000, 'success', NULL, '2021-06-20 11:14:30', '2021-06-27 22:15:58'),
(37, 'Khánh', '0961503015', 'lnminhthu151220@gmail.com', 'jshsftuwmkdonjsd', 15000, 295000, 'success', NULL, '2021-06-20 11:14:53', '2021-06-27 22:15:53'),
(38, 'Thuỷ Trang', '0973776556', 'lnminhthu151220@gmail.com', 'abc dhb dhc', 15000, 135000, 'success', NULL, '2021-06-20 11:15:15', '2021-06-27 22:15:48'),
(39, 'Phương Linh', '0965797635', 'lnminhthu151220@gmail.com', 'abc dhi chn', 0, 320000, 'success', NULL, '2021-06-20 11:16:36', '2021-06-27 22:15:42'),
(40, 'Thuỷ Trang', '0973776556', 'lnminhthu151220@gmail.com', 'abc dhb dhc', 15000, 295000, 'success', NULL, '2021-06-20 11:17:09', '2021-06-27 22:12:35'),
(41, 'Trà Phạm', '0814549770', 'ttnpham18@gmail.com', 'số nhà 184 tổ 23 phường him lam thành phố điện biên phủ tỉnh điện biên', 15000, 135000, 'success', NULL, '2021-07-24 11:34:05', '2021-08-31 21:05:51'),
(42, 'Đỗ Ngọc Lan Thanh', '0973532235', 'momxinh.2308@gmail.com', '23/10 ấp 3 xã phú ngọc huyện định quán tỉnh đồng nai', 15000, 135000, 'success', NULL, '2021-08-25 03:05:24', '2021-08-31 21:05:44'),
(43, 'Danny', '0384837667', 'dannyq2016@yahoo.com', '20 Hoàng Minh Giám, P9, Q. Phú Nhuận', 15000, 615000, 'success', NULL, '2021-08-31 03:35:50', '2021-08-31 21:05:35'),
(44, 'Phan Trần Chí Kiệt', '0973020504', 'kietphan2909@gmail.com', 'HCM', 15000, 615000, 'success', NULL, '2021-08-31 20:48:34', '2021-08-31 21:05:25');

-- --------------------------------------------------------

--
-- Table structure for table `orders_details`
--

CREATE TABLE `orders_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `orders_id` bigint(20) UNSIGNED NOT NULL,
  `products_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders_details`
--

INSERT INTO `orders_details` (`id`, `orders_id`, `products_id`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 1, '2021-04-05 13:37:59', '2021-04-05 13:37:59'),
(2, 2, 2, 1, '2021-04-05 16:56:19', '2021-04-05 16:56:19'),
(3, 3, 4, 1, '2021-04-06 15:02:01', '2021-04-06 15:02:01'),
(4, 4, 1, 1, '2021-06-20 10:21:58', '2021-06-20 10:21:58'),
(5, 4, 2, 1, '2021-06-20 10:21:58', '2021-06-20 10:21:58'),
(6, 5, 4, 1, '2021-06-20 10:22:36', '2021-06-20 10:22:36'),
(7, 6, 2, 3, '2021-06-20 10:24:51', '2021-06-20 10:24:51'),
(8, 7, 3, 1, '2021-06-20 10:25:58', '2021-06-20 10:25:58'),
(9, 8, 4, 1, '2021-06-20 10:27:12', '2021-06-20 10:27:12'),
(10, 9, 1, 1, '2021-06-20 10:31:32', '2021-06-20 10:31:32'),
(11, 10, 1, 2, '2021-06-20 10:32:02', '2021-06-20 10:32:02'),
(12, 11, 3, 1, '2021-06-20 10:32:58', '2021-06-20 10:32:58'),
(13, 11, 2, 1, '2021-06-20 10:32:58', '2021-06-20 10:32:58'),
(14, 12, 4, 1, '2021-06-20 10:33:40', '2021-06-20 10:33:40'),
(15, 13, 1, 1, '2021-06-20 10:34:46', '2021-06-20 10:34:46'),
(16, 13, 3, 1, '2021-06-20 10:34:46', '2021-06-20 10:34:46'),
(17, 14, 4, 1, '2021-06-20 10:37:11', '2021-06-20 10:37:11'),
(18, 15, 1, 1, '2021-06-20 10:37:46', '2021-06-20 10:37:46'),
(19, 16, 1, 2, '2021-06-20 10:38:23', '2021-06-20 10:38:23'),
(20, 17, 3, 2, '2021-06-20 10:39:07', '2021-06-20 10:39:07'),
(21, 18, 2, 5, '2021-06-20 10:40:45', '2021-06-20 10:40:45'),
(22, 19, 1, 1, '2021-06-20 10:43:07', '2021-06-20 10:43:07'),
(23, 20, 3, 1, '2021-06-20 10:44:03', '2021-06-20 10:44:03'),
(24, 20, 4, 1, '2021-06-20 10:44:03', '2021-06-20 10:44:03'),
(25, 21, 4, 1, '2021-06-20 10:45:13', '2021-06-20 10:45:13'),
(26, 22, 1, 3, '2021-06-20 10:59:54', '2021-06-20 10:59:54'),
(27, 23, 4, 2, '2021-06-20 11:01:11', '2021-06-20 11:01:11'),
(28, 24, 1, 1, '2021-06-20 11:04:44', '2021-06-20 11:04:44'),
(29, 24, 2, 1, '2021-06-20 11:04:44', '2021-06-20 11:04:44'),
(30, 25, 2, 1, '2021-06-20 11:06:00', '2021-06-20 11:06:00'),
(31, 26, 4, 1, '2021-06-20 11:07:09', '2021-06-20 11:07:09'),
(32, 27, 4, 1, '2021-06-20 11:07:41', '2021-06-20 11:07:41'),
(33, 28, 1, 1, '2021-06-20 11:08:02', '2021-06-20 11:08:02'),
(34, 29, 3, 1, '2021-06-20 11:09:17', '2021-06-20 11:09:17'),
(35, 29, 2, 1, '2021-06-20 11:09:17', '2021-06-20 11:09:17'),
(36, 30, 1, 1, '2021-06-20 11:10:31', '2021-06-20 11:10:31'),
(37, 30, 2, 1, '2021-06-20 11:10:31', '2021-06-20 11:10:31'),
(38, 31, 3, 1, '2021-06-20 11:10:54', '2021-06-20 11:10:54'),
(39, 32, 4, 1, '2021-06-20 11:12:44', '2021-06-20 11:12:44'),
(40, 33, 1, 1, '2021-06-20 11:13:06', '2021-06-20 11:13:06'),
(41, 34, 3, 2, '2021-06-20 11:13:34', '2021-06-20 11:13:34'),
(42, 35, 1, 2, '2021-06-20 11:13:57', '2021-06-20 11:13:57'),
(43, 36, 2, 2, '2021-06-20 11:14:30', '2021-06-20 11:14:30'),
(44, 37, 1, 1, '2021-06-20 11:14:53', '2021-06-20 11:14:53'),
(45, 38, 2, 1, '2021-06-20 11:15:15', '2021-06-20 11:15:15'),
(46, 39, 3, 1, '2021-06-20 11:16:36', '2021-06-20 11:16:36'),
(47, 39, 2, 1, '2021-06-20 11:16:36', '2021-06-20 11:16:36'),
(48, 40, 1, 1, '2021-06-20 11:17:09', '2021-06-20 11:17:09'),
(49, 41, 2, 1, '2021-07-24 11:34:05', '2021-07-24 11:34:05'),
(50, 42, 2, 1, '2021-08-25 03:05:24', '2021-08-25 03:05:24'),
(51, 43, 4, 1, '2021-08-31 03:35:50', '2021-08-31 03:35:50'),
(52, 44, 4, 1, '2021-08-31 20:48:34', '2021-08-31 20:48:34');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` int(11) NOT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image_3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image_4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image_5` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_price`, `product_description`, `slug`, `product_thumbnail`, `product_avatar`, `product_image_1`, `product_image_2`, `product_image_3`, `product_image_4`, `product_image_5`, `created_at`, `updated_at`) VALUES
(1, 'Dầu Gội Bưởi Vi Jully Cosmetics', 280000, '<div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\">\r\n<p dir=\"auto\"><span style=\"color: #000000;\">Được chiết xuất từ vỏ bưởi da xanh. Như ch&uacute;ng ta đ&atilde; biết trong vỏ bưởi chứa rất nhiều tinh dầu tốt cho t&oacute;c v&agrave; da đầu. Với c&ocirc;ng nghệ ti&ecirc;n tiến khi sản xuất dầu gội Bưởi Vi Jully đ&atilde; giữ được c&aacute;c th&agrave;nh phần tốt nhất để l&agrave;m sạch t&oacute;c, ngăn rụng t&oacute;c hiệu quả, dưỡng da đầu v&agrave; ngăn ngừa h&igrave;nh th&agrave;nh g&agrave;u . Đặc biệt giữ lại được m&ugrave;i thơm đặc trưng của vỏ bưởi, rất nhẹ nh&agrave;ng m&agrave; lưu hương l&acirc;u.</span></p>\r\n</div>\r\n<p>&nbsp;</p>\r\n<h1><span style=\"color: #2b6b34;\">TH&Agrave;NH PHẦN CH&Iacute;NH</span></h1>\r\n<p><span style=\"color: #000000;\">Chiết xuất từ hoa v&agrave; vỏ bưởi, vitamin, collagen.</span></p>\r\n<p>&nbsp;</p>\r\n<h1><span style=\"color: #2b6b34;\">C&Ocirc;NG DỤNG</span></h1>\r\n<p><span style=\"color: #000000;\">Trị rụng t&oacute;c, l&agrave;m chắc khỏe ch&acirc;n t&oacute;c, trị ngứa da đầu, trị g&agrave;u, mềm mượt t&oacute;c, tăng độ đ&agrave;n hồi cho t&oacute;c.</span></p>\r\n<div id=\"eJOY__extension_root\" class=\"eJOY__extension_root_class\" style=\"all: unset;\"></div>', 'dau-goi-buoi', '/images/products/daugoi-thumb.jpg', '/images/products/daugoi-avatar.jpg', '/images/products/daugoi-img.jpg', '/images/products/daugoi-img-2.jpg', '/images/products/daugoi-img-3.jpg', '', '', NULL, '2021-01-17 20:46:00'),
(2, 'Tinh Dầu Bưởi Vi Jully Cosmetics', 120000, '<p><span style=\"color: #000000;\">Chai n&agrave;y tuy nhỏ m&agrave; c&oacute; v&otilde; nha. Chiết xuất từ vỏ bưởi cộng với nước chưng cất vỏ bưởi. Kh&ocirc;ng c&oacute; th&agrave;nh phần h&oacute;a chất n&ecirc;n d&ugrave;ng được cho tất cả mọi người. Ph&ugrave; hợp cho cả mẹ bầu, mẹ bỉm đang cho con b&uacute;.</span></p>\r\n<p>&nbsp;</p>\r\n<h1><span style=\"color: #2b6b34;\">TH&Agrave;NH PHẦN CH&Iacute;NH</span></h1>\r\n<p><span style=\"color: #000000;\">Chiết xuất từ tinh dầu vỏ bưởi nguy&ecirc;n chất.</span></p>\r\n<h1><span style=\"color: #2b6b34;\">C&Ocirc;NG DỤNG</span></h1>\r\n<p><span style=\"color: #000000;\">K&iacute;ch mọc t&oacute;c, trị h&oacute;i, trị rụng t&oacute;c, l&agrave;m d&agrave;i<br />5-8cm/th&aacute;ng - d&agrave;y t&oacute;c, gi&uacute;p ch&acirc;n t&oacute;c chắc khỏe.</span></p>', 'tinh-dau-buoi', '/images/products/xitbuoi-thumb.jpg', '/images/products/xitbuoi-avatar.jpg', '/images/products/xitbuoi-img.jpg', '/images/products/xitbuoi-img-2.jpg', '/images/products/xitbuoi-img-3.jpg', '', '', NULL, '2021-01-07 15:01:29'),
(3, 'Dầu Xả Dừa & Hoa Cúc Vi Jully Cosmetics', 200000, '<p><span style=\"color: #000000;\">Với m&ugrave;i đặc trưng chiết xuất từ hoa c&uacute;c trắng. Dầu xả Vi Jully phục hồi t&oacute;c kh&ocirc; sơ g&atilde;y rụng, phục hồi cực tốt cho t&oacute;c tả tơi do qu&aacute; tr&igrave;nh l&agrave;m t&oacute;c v&agrave; sử dụng h&oacute;a chất. C&aacute;c Protein c&oacute; trong dầu xả sẽ gi&uacute;p t&oacute;c phục hồi b&oacute;ng khỏe v&agrave; giữ nếp suốt 24h</span></p>\r\n<h1><span style=\"color: #2b6b34;\">TH&Agrave;NH PHẦN CH&Iacute;NH</span></h1>\r\n<p><span style=\"color: #000000;\">Chiết xuất hoa c&uacute;c trắng, tinh dầu dừa, dầu agran, oliu, bơ thực vật.</span></p>\r\n<p>&nbsp;</p>\r\n<h1><span style=\"color: #2b6b34;\">C&Ocirc;NG DỤNG</span></h1>\r\n<p><span style=\"color: #000000;\">Phục hồi t&oacute;c hư tổn, dập n&aacute;t do sử dụng nhiệt, h&oacute;a chất nhiều. T&oacute;c b&igrave;nh thường mềm mượt ngay lần ủ đầu ti&ecirc;n. T&oacute;c sử dụng nhiệt sau lần thứ 3 phục hồi.</span></p>', 'dau-xa-hoa-cuc', '/images/products/dauxa-thumb.jpg', '/images/products/dauxa-avatar.jpg', '/images/products/dauxa-img.jpg', '/images/products/dauxa-img-2.jpg', '/images/products/dauxa-img-3.jpg', '', '', NULL, '2021-01-07 15:04:53'),
(4, 'COMBO VI JULLY COSMETICS', 600000, '<div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\">\r\n<p dir=\"auto\"><span style=\"color: #000000;\">DẦU GỘI BƯỞI: Được chiết xuất từ vỏ bưởi da xanh. Như ch&uacute;ng ta đ&atilde; biết trong vỏ bưởi chứa rất nhiều tinh dầu tốt cho t&oacute;c v&agrave; da đầu. Với c&ocirc;ng nghệ ti&ecirc;n tiến khi sản xuất dầu gội Bưởi Vi Jully đ&atilde; giữ được c&aacute;c th&agrave;nh phần tốt nhất để l&agrave;m sạch t&oacute;c, ngăn rụng t&oacute;c hiệu quả, dưỡng da đầu v&agrave; ngăn ngừa h&igrave;nh th&agrave;nh g&agrave;u . Đặc biệt giữ lại được m&ugrave;i thơm đặc trưng của vỏ bưởi, rất nhẹ nh&agrave;ng m&agrave; lưu hương l&acirc;u.</span></p>\r\n<p dir=\"auto\"><span style=\"color: #000000;\">DẦU XẢ DỪA &amp; HOA C&Uacute;C: Với m&ugrave;i đặc trưng chiết xuất từ hoa c&uacute;c trắng. Dầu xả Vi Jully phục hồi t&oacute;c kh&ocirc; sơ g&atilde;y rụng, phục hồi cực tốt cho t&oacute;c tả tơi do qu&aacute; tr&igrave;nh l&agrave;m t&oacute;c v&agrave; sử dụng h&oacute;a chất. C&aacute;c Protein c&oacute; trong dầu xả sẽ gi&uacute;p t&oacute;c phục hồi b&oacute;ng khỏe v&agrave; giữ nếp suốt 24h.</span></p>\r\n</div>\r\n<div class=\"o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q\">\r\n<p dir=\"auto\"><span style=\"color: #000000;\">TINH DẦU BƯỞI: Chai n&agrave;y tuy nhỏ m&agrave; c&oacute; v&otilde; nha. Chiết xuất từ vỏ bưởi cộng với nước chưng cất vỏ bưởi. Kh&ocirc;ng c&oacute; th&agrave;nh phần h&oacute;a chất n&ecirc;n d&ugrave;ng được cho tất cả mọi người. T&aacute;c dụng ch&iacute;nh l&agrave; k&iacute;ch mọc t&oacute;c con, l&agrave;m d&agrave;y v&agrave; nhanh d&agrave;i t&oacute;c. Hỗ trợ ngăn rụng t&oacute;c, trị h&oacute;i đầu hiệu quả sau 2-3 tuần sử dụng.</span></p>\r\n<div dir=\"auto\">&nbsp;</div>\r\n</div>\r\n<h1><span style=\"color: #2b6b34;\">TH&Agrave;NH PHẦN CH&Iacute;NH</span></h1>\r\n<p><span style=\"color: #000000;\">- Chiết xuất từ tinh dầu vỏ bưởi nguy&ecirc;n chất.</span><br /><span style=\"color: #000000;\">- Chiết xuất từ hoa v&agrave; vỏ bưởi, vitamin, collagen.</span><br /><span style=\"color: #000000;\">- Chiết xuất hoa c&uacute;c trắng, tinh dầu dừa, dầu agran, oliu, bơ thực vật.</span></p>\r\n<h1><span style=\"color: #2b6b34;\">C&Ocirc;NG DỤNG</span></h1>\r\n<p><span style=\"color: #000000;\">Combo trị rụng tóc của Vi Jully Cosmetic l&agrave; lựa chọn ho&agrave;n hảo cho những chị em mong muốn c&oacute; 1 m&aacute;i t&oacute;c đẹp. Những chị em n&agrave;o t&oacute;c rụng sau sinh, t&oacute;c rối, xơ,&hellip;</span></p>', 'combo-vijully', '/images/products/combo-thumb.jpg', '/images/products/combo-avatar.jpg', '/images/products/combo-img.jpg', '/images/products/combo-img-2.jpg', '/images/products/combo-img-3.jpg', '', '', NULL, '2021-01-07 15:07:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Lê Nguyễn Minh Thư', 'lnminhthu1512@gmail.com', NULL, '$2y$10$SZ3wzXwVzW7QfCgm9JhZlehED1nI7qULeoghHY5Gv/b5LJw.NvHw2', NULL, '2020-10-22 09:20:33', '2020-10-22 09:20:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_details`
--
ALTER TABLE `orders_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_details_orders_id_foreign` (`orders_id`),
  ADD KEY `orders_details_products_id_foreign` (`products_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `orders_details`
--
ALTER TABLE `orders_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders_details`
--
ALTER TABLE `orders_details`
  ADD CONSTRAINT `orders_details_orders_id_foreign` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_details_products_id_foreign` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
