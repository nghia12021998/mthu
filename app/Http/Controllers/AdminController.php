<?php

namespace App\Http\Controllers;

use App\Models\Orders;
use App\Models\OrdersDetails;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        session(['module_ative' => ""]);
        $orders = Orders::where('status', 'waiting')->orderBy('created_at', 'DESC')->limit(10)->get();
        $total_waiting_orders = Orders::where('status', 'waiting')->count();
        $total_success_orders = Orders::where('status', 'success')->count();
        $total_deleted_orders = Orders::onlyTrashed()->count();
        $income = Orders::where('status', 'success')->sum('total');
        return view('admin.dashboard', compact('orders', 'total_waiting_orders', 'total_success_orders', 'total_deleted_orders', 'income'));
    }

    public function view_order($id)
    {
        $order = Orders::withTrashed()->find($id);
        $details = $order->ordersDetails;
        return view('admin.order_detail', compact('order', 'details'));
    }

    public function viewAllWaitingOrders()
    {
        session(['module_ative' => "waittingOrders"]);
        $orders = Orders::where('status', 'waiting')->orderBy('created_at', 'DESC')->simplePaginate(15);
        return view("admin.waiting_orders", compact('orders'));
    }


    public function viewAllSuccessOrders()
    {
        session(['module_ative' => "successOrders"]);
        $orders = Orders::where('status', 'success')->orderBy('created_at', 'DESC')->simplePaginate(15);
        return view("admin.success_orders", compact('orders'));
    }

    public function viewAllDeletedOrders()
    {
        session(['module_ative' => "deletedOrders"]);
        $orders = Orders::onlyTrashed()->orderBy('created_at', 'DESC')->simplePaginate(15);
        return view("admin.deleted_orders", compact('orders'));
    }


    public function finishOrder($id)
    {
        Orders::find($id)->update(['status' => 'success']);
        return back()->with('msg', 'Thao tác thành công');
    }


    public function deleteOrder($id)
    {
        Orders::find($id)->delete();
        return back()->with('msg', 'Thao tác thành công');
    }

    public function restoreOrder($id)
    {
        Orders::onlyTrashed()->find($id)->restore();
        return back()->with('msg', 'Thao tác thành công');
    }

    public function forceDeleteOrder($id)
    {
        Orders::onlyTrashed()->find($id)->forceDelete();
        return redirect()->route('admin.view-deleted-orders');
    }
}
