<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth')->only('viewAll', 'viewProduct');
    }

    function index()
    {
        $products = Products::all();

        return view("pages.product", compact('products'));
    }

    function detail($slug)
    {
        $product = Products::where('slug', $slug)->first();

        if (empty($product)) {
            abort(404);
        }

        return view("pages.product_detail", compact('product'));
    }

    function viewAll()
    {
        session(['module_ative' => "viewAllProducts"]);
        $products = Products::all();
        return view("admin.all_products", compact('products'));
    }

    function viewProduct($id)
    {
        $product = Products::find($id);
        return view("admin.view_product", compact('product'));
    }

    function editProduct(Request $request, $id)
    {
        $request->validate(
            [
                'txtProductName' => 'required',
                'txtProductPrice' => 'required',
                'txtProductDescription' => 'required',
            ],
            [
                'required' => '* :attribute trống!',
            ],
            [
                'txtProductName' => 'Tên sản phẩm',
                'txtProductPrice' => 'Giá sản phẩm',
                'txtProductDescription' => 'Mô tả sản phẩm',
            ]
        );

        $product = Products::find($id);

        $product->update([
            'product_name' => $request->txtProductName,
            'product_price' => $request->txtProductPrice,
            'product_description' => $request->txtProductDescription,
        ]);

        return back()->with('msg', 'Lưu thành công');
    }
}
