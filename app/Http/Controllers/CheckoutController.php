<?php

namespace App\Http\Controllers;

use App\Mail\CheckoutMail;
use App\Mail\NewOrderMail;
use App\Models\Orders;
use App\Models\OrdersDetails;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CheckoutController extends Controller
{
    var $shipping_fee = 15000;

    function index()
    {
        $product_list = Cart::content();
        $subtotal = Cart::subtotal();
        $total = Cart::total();

        if (Cart::count() >= 2) {
            $this->shipping_fee = 0;
        }

        $total += $this->shipping_fee;

        $shipping_fee = $this->shipping_fee;

        if (empty($product_list->first())) {
            return redirect()->route('cart');
        }
        return view('pages.checkout', compact('product_list', 'subtotal', 'shipping_fee', 'total'));
    }

    function buy(Request $request)
    {
        $total = Cart::total();
        if (Cart::count() >= 2) {
            $this->shipping_fee = 0;
        }

        $request->validate(
            [
                'fullname' => 'required',
                'phoneNumber' => 'required|regex:/^(0)+([0-9]{9})$/',
                'email' => 'required|email',
                'address' => 'required'
            ],
            [
                'required' => '* :attribute trống!',
                'regex' => '* :attribute không hợp lệ!',
                'email' => '* :attribute không hợp lệ!'
            ],
            [
                'fullname' => 'Họ và tên',
                'phoneNumber' => 'Số điện thoại',
                'email' => 'Email',
                'address' => 'Địa chỉ giao hàng'
            ]
        );


        // insert to Orders db 
        $order = new Orders();
        $order->customer_name = $request->fullname;
        $order->customer_phoneNumber = $request->phoneNumber;
        $order->customer_email = $request->email;
        $order->customer_address = $request->address;
        $order->shipping_fee = $this->shipping_fee;
        $order->total = $total + $this->shipping_fee;
        $order->status = 'waiting';
        $order->save();

        // insert to Orders_details db
        foreach (Cart::content() as $item) {
            $details = new OrdersDetails();
            $details->orders_id = $order->id;
            $details->products_id = $item->id;
            $details->quantity = $item->qty;
            $details->save();
        }

        $emailContent = [
            'user' => $request,
            'products' => Cart::content(),
            'shippingFee' => $this->shipping_fee,
            'total' => $total + $this->shipping_fee
        ];

        // lnminhthu1512@gmail.com
        Mail::to($request->email)->send(new CheckoutMail($emailContent));
        Mail::to('lnminhthu1512@gmail.com')->send(new NewOrderMail($emailContent));
        Mail::to('kietphan2909@gmail.com')->send(new NewOrderMail($emailContent));

        $request->session()->flash('fullname', $request->fullname);
        return redirect()->route('success');
    }
}
