<?php

namespace App\Http\Controllers;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class SuccessController extends Controller
{
    //

    function index()
    {
        if (session('fullname')) {
            Cart::destroy();
            return view('pages.success', ['fullname' => session('fullname')]);
        } else {
            return redirect()->route('index');
        }
    }
}
