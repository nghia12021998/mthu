<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
  var $shipping_fee = 0;
  //
  function index()
  {
    if (Cart::count() < 2) {
      $this->shipping_fee = 15000;
    }
    // Cart::destroy();
    $product_list = Cart::content();
    $subtotal = Cart::subtotal();
    $shipping_fee = $this->shipping_fee;
    $total = Cart::total() + $shipping_fee;


    return view("pages.cart", compact('product_list', 'subtotal', 'shipping_fee', 'total'));
  }

  function add($id)
  {
    //find product by id
    $product = Products::find($id);
    // Cart::destroy();
    if (!empty($product)) {
      Cart::add(
        [
          'id' => $product->id,
          'name' => $product->product_name,
          'qty' => 1, 'price' => $product->product_price,
          'options' => ['thumbnail' => $product->product_thumbnail, 'slug' => $product->slug]
        ]
      );
    }
    return "";
  }

  function show()
  {
    //get cart content
    $product_list = Cart::content();
    $output = "";
    $buttons = "";

    if (!empty($product_list->first())) {
      foreach ($product_list as $product) {
        $output .= "
        <li class='cart-item'>
        <a href='" . route('products.detail', $product->options['slug']) . "'>
          <div class='product-thumb'>
            <img
              src='" . asset($product->options['thumbnail']) .  "'
              alt='{$product->name}'
            />
          </div>
          <div class='bag-product-info'>
            <p class='product-name'>{$product->name}</p>
            <p class='qty'>
              Số lượng: <span>{$product->qty}</span> × " . number_format($product->price, 0, '', '.') . " VND
            </p>
          </div>
          <div class='product-sub-price'>
            <p>" . number_format($product->subtotal, 0, '', '.') . " VND</p>
          </div>
        </a>
      </li>
        ";
      }

      $buttons .= "
                  <div class='view-cart'>
                      <a href='" . route('cart') . "'>XEM GIỎ HÀNG</a>
                  </div>

                  <div class='checkout-cart'>
                      <a href='" . route('checkout') . "'>THANH TOÁN</a>
                  </div>
        ";
    } else {
      $output .= "<h4 class='empty-bag'>
                    Giỏ hàng trống! <a href=" . route('products') . ">Mua hàng »</a>
                  </h4> ";
    }

    $result = array(
      'output' => $output,
      'buttons' => $buttons,
      'cart_count' => Cart::count(),
      'cart_subtotal' => number_format(Cart::subtotal(), 0, '', '.'),
    );

    return json_encode($result);
  }

  function delete($rowId)
  {
    Cart::remove($rowId);
    return redirect()->route('cart');
  }

  function update($rowId, $qty)
  {
    Cart::update($rowId, $qty);

    if (Cart::count() < 2) {
      $this->shipping_fee = 15000;
    }

    $result = array(
      "product_subtotal" => number_format(Cart::get($rowId)->price * $qty, 0, '', '.'),
      "subtotal" => number_format(Cart::subtotal(), 0, '', '.'),
      "shipping_fee" => number_format($this->shipping_fee, 0, '', '.'),
      "total" => number_format(Cart::total() + $this->shipping_fee, 0, '', '.'),
    );

    return json_encode($result);
  }
}
