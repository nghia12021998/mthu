<?php

namespace App\Http\Controllers;

use App\Mail\RecruitmentMail;
use App\Models\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RecruitmentController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('auth')->only('getAll');
    }

    function index()
    {
        return view("pages.recruitment");
    }

    function join(Request $request)
    {
        $errors = [];
        $success = "";
        if (empty($request->fullname)) {
            $errors["fullname"] =  "* Họ và tên trống!";
        }

        if (empty($request->email)) {
            $errors["email"] =  "* Email trống!";
        } else {
            if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                $errors["email"] =  "* Email không hợp lệ!";
            }
        }

        if (empty($request->phoneNumber)) {
            $errors["phoneNumber"] =  "* Số điện thoại trống!";
        } else {
            if (!preg_match("/^(0)+([0-9]{9})$/", $request->phoneNumber)) {
                $errors["phoneNumber"] =  "* Số điện thoại không hợp lệ!";
            }
        }

        if (empty($errors)) {
            $customer = new Customers();
            $customer->customer_name = $request->fullname;
            $customer->customer_phoneNumber = $request->phoneNumber;
            $customer->customer_email = $request->email;
            $customer->action = "recruitment";
            $customer->save();
            $success = "* Đăng ký thành công.";

            $emailContent = [
                'user' => $request,
            ];
            // lnminhthu1512@gmail.com
            Mail::to("lnminhthu1512@gmail.com")->send(new RecruitmentMail($emailContent));
        }

        $output = array(
            "errors" => $errors,
            "success" => $success,
        );
        return json_encode($output);
    }

    function getAll()
    {
        session(['module_ative' => "getAllRecruitment"]);
        $customers = Customers::where('action', 'recruitment')->orderBy('created_at', 'DESC')->simplePaginate(15);
        return view('admin.all_recruitment_customers', compact('customers'));
    }
}
