<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Models\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    //

    public function index()
    {
        return view('pages.contact');
    }

    public function submit(Request $request)
    {
        $request->validate(
            [
                'txtFullName' => 'required',
                'txtPhoneNumber' => 'required|regex:/^(0)+([0-9]{9})$/',
                'txtEmail' => 'required'
            ],
            [
                'required' => '* :attribute trống!',
                'regex' => '* :attribute không hợp lệ!',
            ],
            [
                'txtFullName' => 'Họ và tên',
                'txtPhoneNumber' => 'Số điện thoại',
                'txtEmail' => 'Email'
            ]
        );

        $customer = new Customers();
        $customer->customer_name = $request->txtFullName;
        $customer->customer_phoneNumber = $request->txtPhoneNumber;
        $customer->customer_email = $request->txtEmail;
        $customer->contact_content = $request->txtContent;
        $customer->action = "contact";
        $customer->save();

        $emailContent = [
            'user' => $request,
        ];
        // lnminhthu1512@gmail.com
        Mail::to('lnminhthu1512@gmail.com')->send(new ContactMail($emailContent));

        return back()->with('msg', '* Cám ơn quý khách đã liên hệ với chúng tôi.');
    }

    function getAll()
    {
        session(['module_ative' => "getAllContact"]);
        $customers = Customers::where('action', 'contact')->orderBy('created_at', 'DESC')->simplePaginate(15);
        return view('admin.all_contact_customers', compact('customers'));
    }
}
