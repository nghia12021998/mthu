<?php

namespace App\Http\Controllers;

use App\Models\Feedbacks;
use App\Models\Products;
use Illuminate\Http\Request;

class MainController extends Controller
{
    //
    function index()
    {
        $products = Products::all()->take(3);
        $feedbacks = Feedbacks::offset(0)->limit(2)->get();
        $feedbacks_2 = Feedbacks::offset(2)->limit(2)->get();
        $feedbacks_3 = Feedbacks::offset(4)->limit(2)->get();
        return view("pages.index", compact('products', 'feedbacks', 'feedbacks_2', 'feedbacks_3'));
    }
}
