<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdersDetails extends Model
{
    use HasFactory;

    function orders()
    {
        return $this->belongsTo('App\Models\Orders');
    }

    function products()
    {
        return $this->belongsTo('App\Models\Products');
    }
}
