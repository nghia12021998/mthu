$(document).ready(function() {
    $(".nav-item").click(function() {
        $(this)
            .children("ul")
            .toggleClass("display");
        $(this)
            .children(".angle")
            .toggleClass("rotate");
    });

    $(".unclickable > a").click(function(e) {
        e.preventDefault();
    });

    $(".greeting").click(function() {
        $(".dropdown").toggleClass("show");
    });
});
