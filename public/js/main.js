show_cart_products();
$(document).ready(function() {
    $("#burger").click(function() {
        $(".line1").toggleClass("toggle-line1");
        $(".line2").toggleClass("toggle-line2");
        $(".line3").toggleClass("toggle-line3");
        $("#main-nav").toggleClass("nav-toggle");
        $("#dark-background").toggleClass("display-block");
    });

    $("#dark-background").click(function() {
        $("#main-nav").toggleClass("nav-toggle");
        $(".line1").toggleClass("toggle-line1");
        $(".line2").toggleClass("toggle-line2");
        $(".line3").toggleClass("toggle-line3");
        $(this).toggleClass("display-block");
    });

    $("#bag-icon").click(function(e) {
        $("#cart").toggleClass("visible");
        e.stopPropagation();
    });

    $("html").click(function(e) {
        $("#cart").removeClass("visible");
    });

    $(".owl-carousel").owlCarousel({
        items: 1,
        margin: 35,
        dots: true,
        loop: true,
        autoplay: true,
        autoplayTimeout: 5000
    });

    $("#product-btn-add-1").click(function(e) {
        $("#cart").addClass("visible");
        $("html, body")
            .stop()
            .animate({ scrollTop: 0 }, 500, "swing");

        $("#loading-bag").removeClass("d-none");
        $("#loading-bag")
            .delay(1500)
            .fadeOut("slow", function() {
                $(this).css("display", "flex");
                $(this).addClass("d-none");
            });
        e.stopPropagation();
    });

    $("#product-btn-add-2").click(function() {
        $(".line1").toggleClass("toggle-line1");
        $(".line2").toggleClass("toggle-line2");
        $(".line3").toggleClass("toggle-line3");
        $("#main-nav").toggleClass("nav-toggle");
        $("#dark-background").toggleClass("display-block");
    });

    //Join
    $("#joinForm").submit(function(e) {
        e.preventDefault();
        var fullname = $(".txtFullName").val();
        var email = $(".txtEmail").val();
        var phoneNumber = $(".txtPhoneNumber").val();

        var data = {
            fullname: fullname,
            email: email,
            phoneNumber: phoneNumber
        };
        $(".btnJoin").prop("disabled", true);
        $(".btnJoin").val("Vui lòng chờ ...");

        $.ajax({
            type: "GET",
            url: "/tuyen-dung/tham-gia",
            data: data,
            dataType: "json",
            success: function(response) {
                $("#joinForm .errorsContainer").html("");
                if (!jQuery.isEmptyObject(response.errors)) {
                    Object.values(response.errors).forEach(val => {
                        $("#joinForm .errorsContainer").append(
                            "<p class='error text-danger' style='font-size: 1.7rem'>" +
                                val +
                                "</p>"
                        );
                    });
                }
                $("#joinForm .errorsContainer").append(
                    "<p class='text-success' style='font-size: 1.7rem'>" +
                        response.success +
                        "</p>"
                );
                $(".btnJoin").prop("disabled", false);
                $(".btnJoin").val("Xác nhận");
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.state);
                alert(thrownError);
            }
        });
    });
    // Add To Cart

    $("#product-btn-add-1").click(function(e) {
        e.preventDefault();
        var id = $(this).attr("product-id");
        setTimeout(function() {
            $.ajax({
                method: "GET",
                url: "/gio-hang/them/" + id,
                dataType: "text",
                success: function(data) {
                    // $("#cart-items ul").html(data);
                    if (data !== "NotReady") {
                        show_cart_products();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.state);
                    alert(thrownError);
                }
            });
        }, 1500);
    });

    $("#product-btn-add-2").click(function(e) {
        e.preventDefault();
        var id = $(this).attr("product-id");
        $.ajax({
            method: "GET",
            url: "/gio-hang/them/" + id,
            dataType: "text",
            success: function(data) {
                // $("#cart-items ul").html(data);
                if (data !== "NotReady") {
                    show_cart_products();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.state);
                alert(thrownError);
            }
        });
    });

    $(".td-product-qty").change(function(e) {
        var id = $(this).attr("data-id");
        var qty = $(this).val();
        var data = { id: id, qty: qty };
        $("#loading-cart").removeClass("d-none");
        $("#loading-cart")
            .delay(1500)
            .fadeOut("slow", function() {
                $(this).css("display", "flex");
                $(this).addClass("d-none");
            });
        e.preventDefault();
        if (qty == 0) {
            window.location.replace(
                window.location.origin + "/gio-hang/delete/" + id
            );
        }

        setTimeout(function() {
            $.ajax({
                method: "GET",
                url:
                    window.location.origin +
                    "/gio-hang/update/" +
                    id +
                    "/" +
                    qty,
                data: data,
                dataType: "json",
                success: function(data) {
                    if (data !== "NotReady") {
                        $("#" + id).text(data.product_subtotal + " VND");
                        $("#products-subtotal").text(data.subtotal + " VND");
                        $("#products-total").text(data.total + " VND");

                        if (data.shipping_fee != 0) {
                            $(".shipping").html(
                                data.shipping_fee +
                                    "VND" +
                                    "<p class='free-shipping'>Freeship khi mua từ 2 sản phẩm.</p>"
                            );
                        } else {
                            $(".shipping").text(data.shipping_fee + " VND");
                        }
                        show_cart_products();
                    }
                }
            });
        }, 1600);
    });
});

function show_cart_products() {
    $.ajax({
        method: "GET",
        url: "/gio-hang/show/",
        dataType: "json",
        success: function(data) {
            $("#cart-items ul").html(data.output);
            $("#cart-btn").html(data.buttons);
            $("#bag-qty").html(data.cart_count);
            $("#cart-qty span").html(data.cart_count);
            $("#subtotal-price").html(data.cart_subtotal + " VND");
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(xhr.state);
            alert(thrownError);
        }
    });
}

function checkout() {
    $("#check-out-btn").prop("disabled", true);
    $("#check-out-btn").val("Vui lòng chờ ...");
    $("#checkout-form").submit();
}

function contact() {
    $(".btnContact").prop("disabled", true);
    $(".btnContact").val("Vui lòng chờ ...");
    $("#contact-form").submit();
}
